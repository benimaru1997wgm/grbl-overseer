#include "grblinstruction.h"
#include "grbldefinitions.h"

#include <gcode/gcodedefinitions.h>
#include <QStringList>

const char* GrblInstruction::s_blockingInstructionsList[] = {INSTRUCTIONS_BLOCKING};

GrblInstruction::GrblInstruction(QString instruction, int lineNumber):
    m_status(Status_Ready),
    m_instructionBytes(instruction.toLatin1()),
    m_lineNumber(lineNumber)
{
    //Remove any Grbl realtime command (here we considere that the stirng might not contain non-ascii chars)
    //Maybe it is a bad idea and will need fix later
    m_instructionBytes.replace(CMD_FEEDHOLD_CHAR,"");
    m_instructionBytes.replace(CMD_RESUME_CHAR,"");
    m_instructionBytes.replace(CMD_STATUS_REQ_CHAR,"");
    m_instructionBytes.replace(CMD_SOFT_RESET_CHAR,"");

    m_instructionBytes.simplified();

    m_instructionBytes.append(END_OF_INSTRUCTION);

    m_isBlocking = isInherentlyBlocking();
}

bool GrblInstruction::isInherentlyBlocking(){
    int blockingInstructionCount = sizeof(s_blockingInstructionsList)/sizeof(s_blockingInstructionsList[0]);
    for(int i = 0 ; i < blockingInstructionCount ; i++){
        if(m_instructionBytes.startsWith(s_blockingInstructionsList[i])){
            return true;
        }
    }
    return false;
}

bool GrblInstruction::compareIgnoreSpace(const char *compChar) const
{
    for(QByteArray::const_iterator localChar = m_instructionBytes.cbegin();
        localChar != m_instructionBytes.cend();
        localChar++, compChar++)
    {
        //Ignore blank local char
        if((*localChar) == GCODE_SPACE) continue;

        //Only do somthing if chars are different
        if((*localChar) != (*compChar) )
        {
            //Both ends reached = IDENTICAL
            if( ( (*compChar) == '\0' ) && ( (*localChar) == END_OF_INSTRUCTION))
                return true;

            //else, different
            else break;
        }
    }

    return false;
}

//QByteArray GrblInstruction::getSimplifiedBytes() const
//{
//    QByteArray simplifiedBytes = m_instructionBytes;
//    simplifiedBytes.replace(' ', "");
//    return simplifiedBytes;
//}

void GrblInstruction::forceBlocking(){
    m_isBlocking = true;
}

bool GrblInstruction::isGrblSpecific() const
{
    return m_instructionBytes.startsWith(GRBL_INST_STARTS_WITH);
}

bool GrblInstruction::isConfigurationSet() const
{
    //Must be of format $X=Y\n
    if(!isGrblSpecific() || getLength() < 4) //Minimum 4 chars : $X=\n
    {
        return false;
    }

    //Second character must be a digit
    const char secondChar = m_instructionBytes.at(1);
    return (secondChar >= '0') && (secondChar <='9');
}

bool GrblInstruction::isConfigurationFetch() const
{
    return isGrblSpecific() && compareIgnoreSpace(GRBL_INST_GET_PARAMS);
}

bool GrblInstruction::isProgramEnd() const
{
    return compareIgnoreSpace(GRBL_INST_PROGRAM_END);
}

bool GrblInstruction::isJog() const
{
    return m_instructionBytes.startsWith(GRBL_INST_JOG);
}

QString GrblInstruction::getStringWithLineNumber(){
    QString returnString = QString::fromLatin1(m_instructionBytes);
    returnString.remove(END_OF_INSTRUCTION);

    if(m_lineNumber > 0){
        QString argString("Line %1 : %2");
        returnString = argString.arg(m_lineNumber).arg(returnString);
    }

    return returnString;
}
