#include "grbljogactivity.h"

#include "../grblboard.h"
#include "../grblengine.h"
#include "../grbldefinitions.h"
#include "../grblconfigiface.h"

#include <QDebug>

GrblJogActivity::GrblJogActivity(int plannerBufferDepth, GrblBoard *parentBoard) :
    GrblAbstractActivity(parentBoard)
{
    m_jogVector = QVector3D();

    //No planner buffer depth info, use default value
    if(plannerBufferDepth == -1) plannerBufferDepth = GRBL_DEFAULT_PLANNER_SIZE;

    // According to GRBL documentation : dt > v^2 / (2 * a * (N-1))
    // lets precompute (2 * (N-1)) ( and avoid negativ values)
    m_accelMultiplier = 2.0f * qMax(0, plannerBufferDepth - 1);
}

void GrblJogActivity::jog(QVector3D jogVector)
{
    //qDebug() << "Jog " << jogVector;

    //Jog can always be cancelled
    if(jogVector.isNull() && !m_jogVector.isNull())
    {
        m_jogVector = QVector3D();
        emit rtcmdToEngine(GrblEngine::RtCmd_JogCancel);
        this->deleteLater();
    }
    //Save clamped jog vector, but ensure its components are between -1.0 and 1.0
    else
    {
        for(uint8_t i = 0 ; i < 3 ; i++)
        {
            m_jogVector[i] = qBound(-1.0f, jogVector[i], 1.0f);
        }
    }
}



GrblInstructionPointer GrblJogActivity::getNextInstruction()
{
    GrblBoard *board = qobject_cast<GrblBoard*>(parent());

    //If not jogging or no parent board, don't need to go further
    if(m_jogVector.isNull() || !board)
    {
        return GrblInstructionPointer();
    }

    GrblConfigIface *boardConfig = board->configuration();

    //Jog string. G21 = Move in millimeter mode, G91 = relative movement
    QString jogString = QStringLiteral(GRBL_INST_JOG "G21 G91 X%1 Y%2 Z%3 F%4");

    //Compute the speed we want par axis (product of seek rates and jog vector)
    QVector3D ratesVector = m_jogVector;
    ratesVector *= boardConfig->getSeekRates();

    //Compute how long the jog should last (cf GRBL jog documentation)
    float deltaTime = 0;    //in seconds
    QVector3D maxAccPerAxis = boardConfig->getMaxAccs();
    for(uint8_t i = 0 ; i < 3 ; i++)
    {
        float maxAccOnCurrAxis = maxAccPerAxis[i];
        float moveRateOnCurrAxis = ratesVector[i];

        //If acceleration <= 0 , no need to compute, would lead a div by zero error
        if(maxAccOnCurrAxis <= 0.0f) continue;

        //According to GRBL documentation : dt > v^2 / (2 * a * (N-1))
        float deltaTimeOnAxis = ( moveRateOnCurrAxis * moveRateOnCurrAxis );
        deltaTimeOnAxis /= (maxAccOnCurrAxis * m_accelMultiplier);

        //Now weight it with the speed factor for this axis.
        //Hence moving at lower speed means smaller deltaTime,
        //leading to slower movement but QUICKER RESPONSE TIME
        deltaTimeOnAxis *= qAbs(m_jogVector[i]);

        //If computed delta time is worst than previous one, keep this one
        if(deltaTimeOnAxis > deltaTime)
        {
            deltaTime = deltaTimeOnAxis;
        }
    }

    deltaTime += (GRBL_EXEC_TIME_MS / 1000.0f); //Add to movement time the time required by grbl to process the instruction

    //Compute the distance we need to move per axis and the global move rate (in mm/s)
    QVector3D moveVector = ratesVector.normalized() * deltaTime;
    float jogRate = ratesVector.length() * 60.0f; //ratesVector is in mm/s, we want mm/min

    //Now complete string values
    jogString = jogString.arg(QString::number(moveVector.x(),'f'),
                              QString::number(moveVector.y(),'f'),
                              QString::number(moveVector.z(),'f'),
                              QString::number(jogRate,'f'));

    //qDebug() << "Jog instruction : "<< jogString;

    return craftInstruction(jogString);
}
