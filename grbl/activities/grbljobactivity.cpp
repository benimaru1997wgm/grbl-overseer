#include "grbljobactivity.h"

#include "../grblboard.h"
#include "../grblengine.h"
#include "../grbldefinitions.h"
#include "../grblstatusiface.h"

#include <gcode/gcodejob.h>

#include <QDebug>

GrblJobActivity::GrblJobActivity(GCodeJob *job, QVector3D currMachPosition, GrblBoard *parent) :
    GrblAbstractActivity(parent),
    m_sendSetOffsetCommand(true),
    m_holdOnError(false),
    m_isLastInstructionProgramEnd(false),
    m_jobPtr(job),
    m_jobOffset(currMachPosition - m_jobPtr->getOrigin() )
{

}

const GCodeJob *GrblJobActivity::getJobPtr() const
{
    return m_jobPtr.data();
}

void GrblJobActivity::setHoldOnError(bool holdOnError)
{
    m_holdOnError = holdOnError;
}

void GrblJobActivity::onInstructionSent(GrblInstructionPointer instPtr)
{
    if(m_sendSetOffsetCommand) m_sendSetOffsetCommand = false;
    else
    {
        updateJobLineStatus(instPtr);
        m_isLastInstructionProgramEnd = instPtr->isProgramEnd();
    }
}

void GrblJobActivity::onInstructionCompleted(GrblInstructionPointer instPtr)
{
    GrblInstruction::Status instStatus = instPtr->getStatus();

    //If required, on error send a feed hold command
    //and a status request to immediately know what is up
    if(m_holdOnError && instStatus == GrblInstruction::Status_Error)
    {
        emit rtcmdToEngine(GrblEngine::RtCmd_FeedHold);
        emit rtcmdToEngine(GrblEngine::RtCmd_StatusRequest);
    }

    //If we still have a job to run
    if(!m_jobPtr.isNull() &&  instStatus != GrblInstruction::Status_Aborted  )
    {
        //update corresponding job line status
        updateJobLineStatus(instPtr);
    }
}

void GrblJobActivity::onCompleted()
{
    m_jobPtr->markAsCompleted();
    emit jobCompleted(m_jobPtr);
}

void GrblJobActivity::updateJobLineStatus(GrblInstructionPointer instPtr)
{
    //Convert GrblInstruction status to GCodeJobLine status
    static const QMap<GrblInstruction::Status, GCodeJobLine::Status> instToLineStatusMap =
            QMap<GrblInstruction::Status, GCodeJobLine::Status>(
    {
        {GrblInstruction::Status_Sent   , GCodeJobLine::Line_Sent       },
        {GrblInstruction::Status_Ok     , GCodeJobLine::Line_Accepted   },
        {GrblInstruction::Status_Error  , GCodeJobLine::Line_Refused    },
        {GrblInstruction::Status_Aborted, GCodeJobLine::Line_Queued     },
        {GrblInstruction::Status_Ready  , GCodeJobLine::Line_Queued     },
    });

    int lineNumber = instPtr->getLineNumber();
    GrblInstruction::Status status = instPtr->getStatus();

    if(!m_jobPtr.isNull() && lineNumber >= 0)
    {
        m_jobPtr->updateLineStatus(lineNumber,
                                   instToLineStatusMap.value(status));

        //Flag line as source of error.
        if(status == GrblInstruction::Status_Error)
        {
            m_jobPtr->flagLine(lineNumber);
        }
    }
}



GrblInstructionPointer GrblJobActivity::getNextInstruction()
{
    //Ensure job is still valid !
    if(m_jobPtr.isNull())
    {
        return GrblInstructionPointer();
    }

    if(m_sendSetOffsetCommand)
    {
        return craftSetOriginInstruction();
    }
    else
    {
        return craftNextJobInstruction();
    }
}

GrblInstructionPointer GrblJobActivity::craftNextJobInstruction()
{
    const GCodeJobLine* nextLinePtr;
    QString nextLineString;

    //Skip any non useful line
    while(1)
    {
        nextLinePtr = m_jobPtr->getLineToRun();

        if(!nextLinePtr)
        {
            //End of job. If job has not sent M2 by itself, we send it
            // (because it is needed to ensure machine will come to a stop
            // before executing the next job, and to reset some GCODE states )
            return m_isLastInstructionProgramEnd
                    ? GrblInstructionPointer()
                    : craftInstruction(GRBL_INST_PROGRAM_END, m_jobPtr->getLineCount());
        }

        nextLineString = nextLinePtr->getSimplifiedString();

        if(!nextLineString.isEmpty())   break;

        m_jobPtr->updateLineStatus(nextLinePtr->getLineNumber(),
                                   GCodeJobLine::Line_Accepted);
    }

    //Generate the instruction
    GrblInstructionPointer craftedInstruction =
            craftInstruction(nextLineString, nextLinePtr->getLineNumber());

    // Instruction flagged as error source are sent using blocking protocol
    // For quicker response to error
    if(nextLinePtr->isFlagged())
    {
        craftedInstruction->forceBlocking();
    }

    return craftedInstruction;
}

GrblInstructionPointer GrblJobActivity::craftSetOriginInstruction()
{
    //Instruction set the offset position, in mm
    //(default Grbl parser state, so has no impact on gcode program)
    QString offsetInstructionString("G21 G92 X%1 Y%2 Z%3");

    for(quint8 i = 0 ; i < 3 ; i ++)
    {
        QString axisOffsetString = QString::number(m_jobOffset[i], 'f'); //Must use 'f' format, grbl can NOT parse 'e' format
        offsetInstructionString = offsetInstructionString.arg(axisOffsetString);
    }

    return craftInstruction(offsetInstructionString, -1);
}

