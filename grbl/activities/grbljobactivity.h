#ifndef GRBLJOBACTIVITY_H
#define GRBLJOBACTIVITY_H

#include "grblabstractactivity.h"

#include <QPointer>
#include <QVector3D>

class GrblBoard;
class GCodeJob;

class GrblJobActivity : public GrblAbstractActivity
{
    Q_OBJECT

public:
    explicit GrblJobActivity(GCodeJob *job, QVector3D currMachPosition, GrblBoard *parent);

    const GCodeJob *getJobPtr() const;

    void setHoldOnError(bool holdOnError);

    void onInstructionSent(GrblInstructionPointer instPtr) override;
    void onInstructionCompleted(GrblInstructionPointer instPtr) override;
    void onCompleted() override;

    virtual GrblInstructionPointer getNextInstruction() override;

signals:
    void jobCompleted(GCodeJob* job);

private:
    void updateJobLineStatus(GrblInstructionPointer instPtr);

    GrblInstructionPointer craftNextJobInstruction();
    GrblInstructionPointer craftSetOriginInstruction();

    bool m_sendSetOffsetCommand;
    bool m_holdOnError;
    bool m_isLastInstructionProgramEnd;

    QPointer<GCodeJob> m_jobPtr;

    QVector3D m_jobOffset;
};

#endif // GRBLJOBACTIVITY_H
