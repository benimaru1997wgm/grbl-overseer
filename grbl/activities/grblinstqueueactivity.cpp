#include "grblinstqueueactivity.h"
#include "../grbldefinitions.h"

#include <QDebug>

GrblInstQueueActivity::GrblInstQueueActivity(QVector<QString> instString, GrblBoard *parent) :
    GrblAbstractActivity(parent),
    m_currentIndex(0)
{
    bool containsNoActualGCode = true;

    //convert all strings to instructions
    m_instructions.reserve(instString.size());
    for(QVector<QString>::const_iterator i = instString.cbegin();
        i != instString.cend();
        i++)
    {
        GrblInstructionPointer craftedInstruction = craftInstruction(*i);
        m_instructions.append(craftedInstruction);

        containsNoActualGCode &= craftedInstruction->isGrblSpecific();
    }

    // If it contains GCode, we must add instruction to reset GCode parser
    // It also serves as a barrier : it won't be completed until machine stops
    if(!containsNoActualGCode)
    {
        m_instructions.append(craftInstruction(GRBL_INST_BARRIER));
    }
}

void GrblInstQueueActivity::onInstructionSent(GrblInstructionPointer)
{
    m_currentIndex++;
}

void GrblInstQueueActivity::onCompleted()
{
    emit completed();
}

GrblInstructionPointer GrblInstQueueActivity::getNextInstruction()
{
    return (m_currentIndex < m_instructions.size())
            ? m_instructions.at(m_currentIndex)
            : GrblInstructionPointer();
}
