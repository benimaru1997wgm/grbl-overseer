import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import GrblStatus 1.0

import StyleFile 1.0


ProgressBar {
    id: topBar

    value: jobManager.totalProgression

    property color bgColor: Style.topbar.bgColor.defaultColorBg
    property color pgColor: Style.topbar.bgColor.defaultColorBg

    style:ProgressBarStyle {
        background: Rectangle {
            color : topBar.bgColor
        }
        progress: Rectangle {
            color : topBar.pgColor
        }
    }

    states: [
        State {
            name: "state_offline"
            when: grblStatus.currentState === GrblStatus.State_Offline
            PropertyChanges { target: topBar  ; bgColor: Style.topbar.bgColor.offlineBg }
            PropertyChanges { target: topBar  ; pgColor: Style.topbar.bgColor.offlinePg }
        },
        State {
            name: "state_unknown"
            when: grblStatus.currentState === GrblStatus.State_Unknown
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.unknownStateBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.unknownStatePg }
        },
        State {
            name: "state_sleeping"
            when: grblStatus.currentState === GrblStatus.State_Sleep
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.sleepingBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.sleepingPg }
        },
        State {
            name: "state_idle"
            when: grblStatus.currentState === GrblStatus.State_Idle
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.idleBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.idlePg }
        },
        State {
            name: "state_running"
            when: grblStatus.currentState === GrblStatus.State_Run
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.runningBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.runningPg }
        },
        State {
            name: "state_jogging"
            when: grblStatus.currentState === GrblStatus.State_Jog
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.joggingBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.joggingPg }
        },
        State {
            name: "state_homing"
            when: grblStatus.currentState === GrblStatus.State_Home
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.homingBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.homingPg }
        },
        State {
            name: "state_door"
            when: grblStatus.currentState === GrblStatus.State_Door
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.doorBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.doorPg }
        },
        State {
            name: "state_paused"
            when: grblStatus.currentState === GrblStatus.State_Hold
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.pausedBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.pausedPg }
        },
        State {
            name: "state_simulating"
            when: grblStatus.currentState === GrblStatus.State_Check
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.simulatingBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.simulatingPg }
        },
        State {
            name: "state_alarm"
            when: grblStatus.currentState === GrblStatus.State_Alarm
            PropertyChanges { target: topBar   ; bgColor: Style.topbar.bgColor.alarmBg }
            PropertyChanges { target: topBar   ; pgColor: Style.topbar.bgColor.alarmPg }
        }
    ]

    transitions: [
        Transition {
            ColorAnimation {property: "bgColor" ; duration: 500}
            ColorAnimation {property: "pgColor" ; duration: 500}
        }
    ]

    Item {
        id: statusDisplay
        anchors.fill: parent

        readonly property string    unitString: (grblStatus.isRawPositionInInches ? qsTr(" (in)") : qsTr(" (mm)"))
        readonly property string    posString:  (grblStatus.isRawPositionInWorkCoord ? qsTr("Work. ") : qsTr("Mach. "))
        readonly property vector3d  position:   grblStatus.rawPosition

        TextWithCaption {
            id:grblStateText

            anchors.top: parent.top
            anchors.bottom: parent.bottom

            x: parent.width * 0.75
            width: parent.width * 0.25

            valueText.text: textFromState(grblStatus.currentState)
            valueText.color: Style.topbar.value.color

            captionText.text: qsTr("Grbl State")
            captionText.color: Style.topbar.caption.color

            function textFromState( state )
            {
                switch(state){
                case GrblStatus.State_Idle:
                    return qsTr("Idle");
                case GrblStatus.State_Run:
                    return qsTr("Running");
                case GrblStatus.State_Hold:
                    return qsTr("Paused");
                case GrblStatus.State_Jog:
                    return qsTr("Jogging");
                case GrblStatus.State_Door:
                    return qsTr("Door");
                case GrblStatus.State_Home:
                    return qsTr("Homing");
                case GrblStatus.State_Alarm:
                    return qsTr("Alarm");
                case GrblStatus.State_Check:
                    return qsTr("Simulation");
                case GrblStatus.State_Sleep:
                    return qsTr("Sleeping")
                case GrblStatus.State_Unknown:
                    return qsTr("Status unknown");
                case GrblStatus.State_Offline:
                    return qsTr("Not connected");
                }
            }
        }

        Rectangle {
            id:separator1
            width: 1
            color: Style.separator.color
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height * Style.separator.span

            x: parent.width * 0.75
        }

        TextWithCaption {
            id:zCoordText

            anchors.top: parent.top
            anchors.bottom: parent.bottom

            x: parent.width * 0.5
            width: parent.width * 0.25

            valueText.text: parent.position.z.toFixed(3)
            valueText.color:  Style.topbar.value.color
            captionText.text: parent.posString + "Z" + parent.unitString
            captionText.color:  Style.topbar.caption.color
        }

        Rectangle {
            id:separator2
            width: 1
            color: Style.separator.color
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height * Style.separator.span

            x: parent.width * 0.5
        }

        TextWithCaption {
            id:yCoordText

            anchors.right: separator2.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom

            x: parent.width * 0.25
            width: parent.width * 0.25

            valueText.text: parent.position.y.toFixed(3)
            valueText.color: Style.topbar.value.color
            captionText.text: parent.posString + "Y" + parent.unitString
            captionText.color: Style.topbar.caption.color
        }

        Rectangle {
            id:separator3
            width: 1
            color: Style.separator.color
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height * Style.separator.span

            x: parent.width * 0.25
        }

        TextWithCaption {
            id:xCoordText

            anchors.right: separator3.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom

            x: 0
            width: parent.width * 0.25

            valueText.text: parent.position.x.toFixed(3)
            valueText.color: Style.topbar.value.color
            captionText.text: parent.posString + "X" + parent.unitString
            captionText.color: Style.topbar.caption.color

        }
    }
}
