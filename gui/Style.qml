pragma Singleton
import QtQuick 2.0

QtObject {
    property QtObject text: QtObject {
        property color defaultColor:            "#FFFFFF"
        property color placeholderColor:        "#606060"
    }

    property QtObject customButton: QtObject {
        property color defaultColor:            "#DDDDDD"
        property real textPixelSizeFactor:      0.16
    }

    property QtObject topbar : QtObject {
        property QtObject bgColor : QtObject {
            property color defaultColorBg:      "#4b6966"
            property color defaultColorPg:      defaultColorBg
            property color offlineBg:           defaultColorBg
            property color offlinePg:           defaultColorPg
            property color unknownStateBg:      defaultColorBg
            property color unknownStatePg:      defaultColorPg
            property color sleepingBg:          defaultColorBg
            property color sleepingPg:          defaultColorPg
            property color idleBg:              "#009688"
            property color idlePg:              idleBg
            property color runningBg:           "#00538c"
            property color runningPg:           "#006ab3"
            property color joggingBg:           "#005996"
            property color joggingPg:           joggingBg
            property color homingBg:            "#005996"
            property color homingPg:            homingBg
            property color doorBg:              "#780a0a"
            property color doorPg:              doorBg
            property color pausedBg:            "#49258c"
            property color pausedPg:            "#5c2fb3"
            property color simulatingBg:        "#8c8000"
            property color simulatingPg:        "#b3a300"
            property color alarmBg:             "#b30f0f"
            property color alarmPg:             alarmBg
        }

        property QtObject value: QtObject {
            property color color:               "#FFFFFF"
        }

        property QtObject caption: QtObject {
            property color color:               "#D0D0D0"
        }
    }

    property QtObject sideBar: QtObject {
        property color bgColor:                 "#404244"

        property QtObject buttonColor: QtObject {
            property color jobs:                "#98A9EE"
            property color controls:            "#92BD6c"
            property color monitor:             "#BA6000"
            property color settings:            "#f3BD04"
            property color start:               "#92BD6C"
            property color resume:              "#6A7BC2"
            property color idle:                "#6A7BC2"
            property color reset:               "#E85F5F"
            property color disabled:            "#6F7273"
            property color engaged:             "#262829"
        }
    }

    property QtObject sidePanel: QtObject {
        property color bgColor:                 "#262829"

        property QtObject title: QtObject {
            property color bgColor:             "#191A1B"
            property color textColor:           "#FFFFFF"
        }
    }

    property QtObject separator: QtObject {
        property color color:                   "#88D0D0D0"
        property double span:                   0.8
    }

    property QtObject groupTitle: QtObject {
        property color color:                   "#FFFFFF"
        property real textSizeRatio:            0.6
        property real aspectRatio:              10
    }

    property QtObject input: QtObject {
        property color enabledColor:            "#00E8E8"
        property color disabledColor:           "#747474"
        property color placeholderColor:        "#747474"
        property color bgColor:                 "#44000000"
    }

    property QtObject deployableItem: QtObject {
        property color evenColor:               "#08FFFFFF"
        property color oddColor:                "#10FFFFFF"
        property color deployedColor:           "#1b403c"
    }

    property QtObject controls: QtObject {
        property color bgColor:                 "#404244"
        property color textColor:               "#AAAAAA"
        property color pressedColor:            "#FF0000"
        property color borderColor:             "#00000000"
        property double borderWidth:            0.0
        property color indicatorColor:          "#D02000"
        property color indicatorLockedIconColor:"#DDDDDD"
        property real overrideTextFontSizeFactor:0.05
        property real overrideTextAreaSizeFactor:0.33
    }

    property QtObject scrollBar: QtObject {
        property color color:                   "#60808080"
        property real widthFactor:              (1/64)
    }

    property QtObject jobList: QtObject {
        property QtObject origin: QtObject {
            property real textPixelSizeFactor:      0.05
        }
        property QtObject originAxis: QtObject {
            property real textPixelSizeFactor:      0.1
        }
        property QtObject originValue: QtObject {
            property real textPixelSizeFactor:      0.1
        }
    }

    property QtObject settings: QtObject {
        property real mainHeightFactor:             0.2

        property QtObject indexSection: QtObject {
            property real widthFactor:              0.125
            property real textPixelSizeFactor:      0.06
        }

        property QtObject labelSection: QtObject {
            property real marginsFactor:            0.002
            property real textPixelSizeFactor:      0.06
        }

        property QtObject valueSection: QtObject {
            property real widthFactor:              0.25
            property real textPixelSizeFactor:      0.05
            property real maskIndicatorHeightFactor:0.2
            property real maskMarginsFactor:        0.02
        }

        property QtObject description: QtObject {
            property real textPixelSizeFactor:      0.05
            property real marginsFactor:            0.04
        }
    }

    property QtObject history: QtObject {
        property real mainLineHeightFactor:     1.25
        property real statusAreaWidthFactor:    0.15
        property real statusIconSizeFactor:     0.55
        property real textPixelSizeFactor:      0.06
        property real subTextPixelSizeFactor:   0.05
        property real lineWidthFactor:          0.005

        property QtObject manualCommand : QtObject {
            property real aspectRatio:          0.10
            property real textPixelSizeRatio:   0.06
        }
    }

    property QtObject customDialog: QtObject {
        property real titlePixelSizeFactor:     0.04
        property real textPixelSizeFactor:      0.03
        property real infoTextPixelSizeFactor:  0.02
        property real detailTextPixelSizeFactor:0.02
        property real maxDetailTextHeightFactor:0.3
        property color dialogColor:             "#202122"
        property color bgColor:                 "#AA000000"
        property color textColor:               "#D0D0D0"
        property real borderHeightfactor:       0.02
        property real spacingFactor:            0.01

        property QtObject buttons: QtObject {
            property real heightFactor:         0.075
            property real aspectRatio:          2.5
            property real textPixelSizeRatio:   0.5
            property color bgColor:             "#A0000000"
            property color pressedBgColor:      "#A0FFFFFF"
            property color textColor:           "#FFFFFFFF"
            property color pressedTextColor:    "#FF000000"
        }
    }
}

