import QtQuick 2.0

import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import StyleFile 1.0

Item {
    anchors.fill: parent

    ItemGroup {
        id: linkSettingsGroup
        title: qsTr("Serial Link")
        contentHeight: width * 0.25
        anchors.top: parent.top

        CustomComboBox{
            id: portComboBox
            anchors.left:  parent.left
            anchors.top: parent.top
            anchors.bottom: parent.verticalCenter
            anchors.right: connectButton.left

            anchors.leftMargin:     parent.width * 0.01
            anchors.rightMargin:    parent.width * 0.01
            anchors.topMargin:      parent.width * 0.005
            anchors.bottomMargin:   parent.width * 0.005

            model:linkManager.portList
            enabled: !linkManager.linkOpen
        }

        CustomComboBox{
            id: baudrateComboBox
            anchors.left:  parent.left
            anchors.top: parent.verticalCenter
            anchors.bottom: parent.bottom
            anchors.right: connectButton.left

            anchors.leftMargin:     parent.width * 0.01
            anchors.rightMargin:    parent.width * 0.01
            anchors.topMargin:      parent.width * 0.005
            anchors.bottomMargin:   parent.width * 0.005

            model: linkManager.standardBaudratesList
            enabled: !linkManager.linkOpen
        }

        CustomButton{
            id:connectButton
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            width: parent.height

            anchors.leftMargin:     parent.width * 0.01
            anchors.rightMargin:    parent.width * 0.01
            anchors.topMargin:      parent.width * 0.005
            anchors.bottomMargin:   parent.width * 0.005

            overlayColorIdle: "transparent"
            overlayColorPressed: "transparent"

            imageSource: linkManager.linkOpen ? "qrc:/icons/link/Connected" : "qrc:/icons/link/Unconnected";

            caption: linkManager.linkOpen ? qsTr("Disconnect") : qsTr("Connect");

            onClicked: {
                linkManager.linkOpen
                        ? linkManager.closeLink()
                        : linkManager.openLink(portComboBox.currentIndex,
                                               baudrateComboBox.currentText)
            }
        }
    }


    Rectangle {
        id: separator1
        height: 1
        width: parent.width * Style.separator.span
        color: Style.separator.color
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: linkSettingsGroup.bottom
    }

    ItemGroup {
        id:grblSettingsGroup
        title: qsTr("GRBL Configuration")

        anchors.top: separator1.bottom
        anchors.bottom: parent.bottom


        ListView {
            id: grblSettingsList

            boundsBehavior: Flickable.StopAtBounds
            anchors.fill: parent

            model: grblConfig
            clip:true
            currentIndex: -1

            delegate: GRBLSettingsDelegate {
                id: settingItem
                width: parent.width
            }

            visible: grblSettingsList.count > 0

            ScrollBar {}
        }

        Text {
            id: settingsNotAvailableText

            anchors.fill: parent

            font.pixelSize: parent.width / 10
            text: qsTr("Machine\nconfiguration\nnot available")

            color: Style.text.placeholderColor

            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap

            visible: !grblSettingsList.visible
        }
    }
}
