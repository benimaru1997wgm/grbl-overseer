import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import GCodeJob 1.0

import StyleFile 1.0

DeployableItem {
    id:jobItem

    mainHeight: parent.width / 4
    extendedHeight: mainHeight * 2

    color: Style.deployableItem.oddColor

    mainItem: Rectangle {
        id: jobInfos

        anchors.fill: parent
        color: "transparent"

        ProgressBar {
            id:progressBar

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            height: parent.height / 10

            value: model.progress

            style:ProgressBarStyle {
                background: Rectangle {
                    color : Qt.darker(model.color)
                }
                progress: Rectangle {
                    color : model.color
                }
            }
        }

        Text {
            id: title
            text: model.text
            color: Style.text.defaultColor
            font.family: "Helvetica"
            font.pixelSize: height * 0.65
            anchors.left: parent.left
            anchors.right: jobStateButton.left
            anchors.leftMargin: 8
            anchors.rightMargin: 8
            anchors.top : progressBar.bottom
            height: parent.height * 0.5
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideLeft
        }

        Text {
            id: lineCountText
            text: model.lineCount +
                  qsTr(" lines")

            color: Style.text.defaultColor
            font.family: "Helvetica"
            font.pixelSize: height * 0.5
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.top: title.bottom
            anchors.leftMargin: 8
            verticalAlignment: Text.AlignTop
        }

        Text {
            id: estimatedMachineTimeText
            text: formatMachineTimeString(model.estimatedTime)

            color: Style.text.defaultColor
            font.family: "Helvetica"
            font.pixelSize: height * 0.5
            anchors.right: jobStateButton.left
            anchors.bottom: parent.bottom
            anchors.top: title.bottom
            anchors.rightMargin: 8
            verticalAlignment: Text.AlignTop

            function formatMachineTimeString(milliseconds)
            {
                var minutes = Math.floor(milliseconds / 60000)
                var hours = Math.floor(minutes / 60);

                var timeString = (minutes < 1) ? "~ 1 min" : "> ";

                if (hours   > 0){timeString   += (hours + " h ");}
                if (minutes > 0){timeString   += ((minutes % 60) + " min");}

                return timeString;
            }
        }

        CustomButton{
            id:jobStateButton
            width: parent.height * 0.66
            height: width
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.margins: 8

            imageSource: jobStateToIcon(model.jobState)

            enabled: appLogic.isIdle

            onClicked: jobManager.toggleJobState(model.index)

            function jobStateToIcon(jobState)
            {
                switch(jobState) {
                case Job.Job_Empty:
                    return "qrc:/icons/job/Disabled";
                case Job.Job_CouldNotOpenFile:
                    return "qrc:/icons/job/Disabled";
                case Job.Job_LineTooLong:
                    return "qrc:/icons/job/Disabled";
                case Job.Job_ContainsCoordOffset:
                    return "qrc:/icons/job/Disabled";
                case Job.Job_Ready:
                    return "qrc:/icons/job/Idle";
                case Job.Job_Queued:
                    return "qrc:/icons/job/Waiting";
                case Job.Job_Disabled:
                    return "qrc:/icons/job/Disabled";
                case Job.Job_Running:
                    return "qrc:/icons/job/Running";
                case Job.Job_Completed:
                    return "qrc:/icons/job/Done";
                }
            }
        }
    }

    extendedItem: Item {
        id: jobControls

        anchors.fill: parent

        Item {
            anchors.bottom: parent.verticalCenter
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right

            CustomButton{
                id:jobSetOriginToCurrentPositionButton

                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: buttonSeparator1.left
                anchors.bottomMargin: 4

                imageSource: "qrc:/icons/job/SetOrigin"

                caption: qsTr("Set origin here")

                enabled: appLogic.readyToGo && (model.jobState !== Job.Job_Running)

                onClicked: appLogic.setJobOriginAtCurrentPosition(index)
            }

            Rectangle {
                id:buttonSeparator1

                x: parent.width * (1/3)
                width: 1

                color: Style.separator.color

                height: parent.height * Style.separator.span
                anchors.verticalCenter: parent.verticalCenter
            }

            CustomButton{
                id:jobChangeColor

                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: buttonSeparator1.right
                anchors.right: buttonSeparator2.left


                imageSource: "qrc:/icons/job/ChangeColor"

                caption: qsTr("Change color")

                onClicked: jobManager.changeJobColor(model.index)
            }

            Rectangle {
                id:buttonSeparator2

                x: parent.width * (2/3)
                width: 1

                color: Style.separator.color

                height: parent.height * Style.separator.span
                anchors.verticalCenter: parent.verticalCenter
            }

            CustomButton{
                id:jobDeleteButton

                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: buttonSeparator2.right
                anchors.right: parent.right
                anchors.bottomMargin: 4

                imageSource: "qrc:/icons/job/Delete"

                caption: qsTr("Delete job")

                enabled: (appLogic.isIdle) && (model.jobState !== Job.Job_Running)

                onClicked: removeJob()

                function removeJob()
                {
                    jobItem.ListView.view.currentIndex = -1
                    jobManager.removeJob(index)
                }
            }
        }

        Item {
            anchors.top: parent.verticalCenter
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right

            Text {
                anchors.top: parent.top
                anchors.right: jobOrigins.left
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                width: parent.width * (3/8)
                text: qsTr("Job origin :\n(mm)")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: Style.text.defaultColor
                font.pixelSize: parent.width * Style.jobList.origin.textPixelSizeFactor

            }

            Column {
                id: jobOrigins
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: parent.width * (5/8)
                anchors.right: parent.right

                Loader {
                    id: xAxisSpinbox
                    property string axisLabelText: "X"
                    property int axis: 0
                    sourceComponent: spinbox
                    width: parent.width
                    height: parent.height / 3
                }
                Loader {
                    id: yAxisSpinbox
                    property string axisLabelText: "Y"
                    property int axis: 1
                    sourceComponent: spinbox
                    width: parent.width
                    height: parent.height / 3
                }
                Loader {
                    id: zAxisSpinbox
                    property string axisLabelText: "Z"
                    property int axis: 2
                    sourceComponent: spinbox
                    width: parent.width
                    height: parent.height / 3
                }
            }
        }

        Component {
            id: spinbox

            Item {

                Binding {
                    target: axisDisplay
                    property: 'value'
                    value: [model.jobOrigin.x, model.jobOrigin.y, model.jobOrigin.z][axis]
                }

                Text {
                    id: axisLabel
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    width: parent.width * (1/7)
                    text: axisLabelText
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: Style.text.defaultColor
                    font.pixelSize: parent.width * Style.jobList.originAxis.textPixelSizeFactor
                }

                SpinBox {
                    id: axisDisplay
                    anchors.top: parent.top
                    anchors.left: axisLabel.right
                    anchors.bottom: parent.bottom
                    width: parent.width * (6/7)
                    minimumValue: -999.0
                    maximumValue: 999.0
                    decimals: 3
                    enabled: appLogic.readyToGo && (model.jobState !== Job.Job_Running)
                    style: SpinBoxStyle {
                        textColor: control.enabled ? Style.input.enabledColor : Style.input.disabledColor
                        font.bold: true
                        font.pixelSize: parent.width * Style.jobList.originValue.textPixelSizeFactor
                        background: Rectangle {
                            color: Style.input.bgColor
                        }
                    }
                    onValueChanged: jobManager.setJobOrigin(model.index, Qt.vector3d(axis == 0 ? value : model.jobOrigin.x,
                                                                  axis == 1 ? value : model.jobOrigin.y,
                                                                  axis == 2 ? value : model.jobOrigin.z))
                }
            }
        }

    }
}
