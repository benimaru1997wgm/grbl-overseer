import QtQuick 2.0
import QtQuick.Controls 1.4
import StyleFile 1.0

import QtMultimedia 5.5

Rectangle {

    id: dialog

    readonly property int buttonOk:     1
    readonly property int buttonYes:    2
    readonly property int buttonNo:     4
    readonly property int buttonAbort:  8


    readonly property int typeInfo:     0
    readonly property int typeWarning:  1
    readonly property int typeCritical: 2

    property alias title: title.text
    property alias text: message.text
    property alias informativeText: informativeMessage.text
    property alias detailedText: detailedMessage.text

    property int type: typeInfo

    property int buttons: buttonOk

    property bool implicitVisible: false

    signal yes
    signal no
    signal accepted
    signal rejected

    function open() {
        implicitVisible = true
    }
    function close() {
        implicitVisible = false
    }

    implicitWidth: 640
    implicitHeight: 480

    visible: implicitVisible

    anchors.fill: parent

    color: Style.customDialog.bgColor

    onVisibleChanged:
    {
        if(visible) {
            dialogOpenSound.play();
            mainWindow.raise();
        }
    }

    SoundEffect {
        id: dialogOpenSound
        source: selectedSoundFromDialogType()
        function selectedSoundFromDialogType()
        {
            switch(dialog.type)
            {
            case dialog.typeInfo:
                return "qrc:/sound/chime";
            case dialog.typeWarning:
                return "qrc:/sound/error";
            case dialog.typeCritical:
                return "qrc:/sound/error";
            }
        }
    }

    MouseArea {
        anchors.fill: parent

        acceptedButtons: Qt.AllButtons
        preventStealing: true
        propagateComposedEvents: false

        onWheel: {
            wheel.accepted = true
        }
    }

    Rectangle {
        id:background
        anchors.fill: contentColumn
        color: Style.customDialog.dialogColor
    }

    Component {
        id: borderImage
        Image {
            height: Style.customDialog.borderHeightfactor * dialog.height
            width: dialog.width
            fillMode: Image.Tile
            source: {
                if(dialog.type === dialog.typeInfo) {
                    return "qrc:/assets/background/info_strip";
                }
                else if (dialog.type === dialog.typeWarning) {
                    return "qrc:/assets/background/warning_strip";
                }
                else if (dialog.type === dialog.typeCritical) {
                    return "qrc:/assets/background/error_strip";
                }
                else {
                    return "";
                }
            }
        }
    }

    Component {
        id:dialogButton

        Rectangle {
            id: button
            implicitWidth: 100
            implicitHeight: 25

            height: dialog.height * Style.customDialog.buttons.heightFactor
            width: height * Style.customDialog.buttons.aspectRatio


            color: clickArea.pressed
                   ? Style.customDialog.buttons.pressedBgColor
                   : Style.customDialog.buttons.bgColor

            MouseArea {
                id: clickArea
                anchors.fill: parent
                propagateComposedEvents: true
                onClicked: buttonAction()
            }

            Text {
                text: buttonText
                clip: true
                wrapMode: Text.WordWrap
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: parent.height * Style.customDialog.buttons.textPixelSizeRatio
                anchors.fill: parent
                color: clickArea.pressed
                       ? Style.customDialog.buttons.pressedTextColor
                       : Style.customDialog.buttons.textColor
            }
        }

    }

    Column {
        id: contentColumn
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        spacing: dialog.height * Style.customDialog.spacingFactor

        Loader {
            id:topBorder
            sourceComponent: borderImage
        }

        Text {
            id: title
            text: ""
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            font.family: "Helvetica"
            font.pixelSize: dialog.height * Style.customDialog.titlePixelSizeFactor
            font.bold: true
            color: Style.customDialog.textColor
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: parent.spacing
        }

        Text {
            id: message
            text: ""
            visible: text !== ""
            wrapMode: Text.WordWrap
            font.family: "Helvetica"
            font.pixelSize: dialog.height * Style.customDialog.textPixelSizeFactor
            color: Style.customDialog.textColor
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: parent.spacing
        }

        Text {
            id: informativeMessage
            text: ""
            visible: text !== ""
            wrapMode: Text.WordWrap
            font.family: "Helvetica"
            font.pixelSize: dialog.height * Style.customDialog.infoTextPixelSizeFactor
            color: Style.customDialog.textColor
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: parent.spacing
        }

        TextArea {
            id: detailedMessage
            text: ""
            visible: text !== ""
            height: getClampedHeight()
            font.family: "Helvetica"
            font.pixelSize: dialog.height * Style.customDialog.detailTextPixelSizeFactor
            textColor: Style.customDialog.textColor
            textFormat: TextEdit.PlainText
            readOnly: true
            backgroundVisible: false
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: parent.spacing

            function getClampedHeight() {
                var maxHeight = Style.customDialog.maxDetailTextHeightFactor * dialog.height;
                return (contentHeight < maxHeight) ? contentHeight : maxHeight;
            }
        }

        Flow {
            id: buttons
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: parent.spacing

            Loader {
                sourceComponent: dialogButton
                active: dialog.buttons & buttonOk
                property string buttonText: qsTr("Ok")
                function buttonAction() { dialog.accepted(); dialog.close(); }
            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & ButtonOpen
//                property string buttonText: qsTr("Open")
//                function buttonAction()  { dialog.accepted(); dialog.close(); }
//            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & ButtonSave
//                property string buttonText: qsTr("Save")
//                function buttonAction()  { dialog.accepted(); dialog.close(); }
//            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & ButtonCancel
//                property string buttonText: qsTr("Cancel")
//                function buttonAction() { dialog.rejected(); dialog.close(); }
//            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & ButtonClose
//                property string buttonText: qsTr("Close")
//                function buttonAction() { dialog.rejected(); dialog.close(); }
//            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & ButtonDiscard
//                property string buttonText: qsTr("Discard")
//                function buttonAction() { /*dialog.discard();*/ dialog.close(); }
//            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & ButtonApply
//                property string buttonText: qsTr("Apply")
//                function buttonAction()  { /*dialog.apply();*/ dialog.close(); }
//            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & ButtonReset
//                property string buttonText: qsTr("Reset")
//                function buttonAction()  { /*dialog.reset();*/ dialog.close(); }
//            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & ButtonRestoreDefaults
//                property string buttonText: qsTr("Restore Defaults")
//                function buttonAction() { /*dialog.reset();*/ dialog.close(); }
//            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & ButtonHelp
//                property string buttonText: qsTr("Help")
//                function buttonAction()  { /*dialog.help();*/ dialog.close(); }
//            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & ButtonSaveAll
//                property string buttonText: qsTr("Save All")
//                function buttonAction() { dialog.accepted(); dialog.close(); }
//            }

            Loader {
                sourceComponent: dialogButton
                active: dialog.buttons & buttonYes
                property string buttonText: qsTr("Yes")
                function buttonAction() { dialog.yes(); dialog.close(); }
            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & buttonYesToAll
//                property string buttonText: qsTr("Yes to all")
//                function buttonAction()  { dialog.yes(); dialog.close(); }
//            }

            Loader {
                sourceComponent: dialogButton
                active: dialog.buttons & buttonNo
                property string buttonText: qsTr("No")
                function buttonAction()  { dialog.no(); dialog.close(); }
            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & buttonNoToAll
//                property string buttonText: qsTr("No to all")
//                function buttonAction() { dialog.no(); dialog.close(); }
//            }

            Loader {
                sourceComponent: dialogButton
                active: dialog.buttons & buttonAbort
                property string buttonText: qsTr("Abort")
                function buttonAction() { dialog.rejected(); dialog.close(); }
            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & ButtonRetry
//                property string buttonText: qsTr("Retry")
//                function buttonAction()  { dialog.accepted(); dialog.close(); }
//            }

//            Loader {
//                sourceComponent: dialogButton
//                active: dialog.buttons & ButtonIgnore
//                property string buttonText: qsTr("Ignore")
//                function buttonAction()  { dialog.accepted(); dialog.close(); }
//            }
        }

        Loader {
            id:bottomBorder
            sourceComponent: borderImage
        }
    }
}
