import QtQuick 2.0
import QtGraphicalEffects 1.0

Rectangle {

    property int retractedWidth: 200
    property int deployedWidth: 400

    default property alias content: flickableContent.data

    id: sidePanel

    width: retractedWidth;

    state:"retracted"

    states: [
        State {
            name: "retracted"
            PropertyChanges { target: sidePanel; width:retractedWidth}
        },
        State {
            name: "deployed"
            PropertyChanges { target: sidePanel; width:deployedWidth}
        }
    ]

    transitions: Transition {
        NumberAnimation { duration: 300; properties: "width"; easing.type: Easing.OutQuad}
    }

    Flickable {
        id:flickableContainer
        anchors.fill: parent
        interactive: false

        contentWidth: flickableContent.width
        contentHeight: flickableContent.height

        Item {
            id:flickableContent

            anchors.left: parent.left
            anchors.bottom: parent.bottom
            width: sidePanel.deployedWidth
            height: sidePanel.height
        }
    }
}


