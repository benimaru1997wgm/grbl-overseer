import QtQuick 2.0

import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

import QtGraphicalEffects 1.0

import AppLogic 1.0


ListView {
    id: jobList
    anchors.fill: parent
    boundsBehavior: Flickable.StopAtBounds

    model: jobManager;
    clip:true
    currentIndex: -1

    onVisibleChanged: {
        currentIndex = -1;
    }

    delegate: JobDelegate {
        id: jobItem
        anchors.left: parent.left
        anchors.right: parent.right
    }

    footer:CustomButton {
        anchors.left: parent.left
        anchors.right: parent.right
        height:jobList.height / 8
        imageSource: "qrc:/icons/add"
        caption: jobList.count > 0 ? qsTr("Load more G-Code jobs") : qsTr("Load G-Code jobs")
        visible: appLogic.isIdle
        onClicked: {
            if(appLogic.isIdle) fileDialog.open();
        }
    }

    onCurrentIndexChanged: {
        model.selectJob(currentIndex)
    }

    add: Transition {
        PropertyAnimation {
            duration: 500
            properties: "opacity"
            from: 0.0
            to: 1.0
        }
    }

    remove: Transition {
        PropertyAnimation {
            duration: 500
            properties: "opacity"
            from: 1.0
            to: 0.0
        }
    }

    displaced: Transition {
            NumberAnimation { properties: "opacity"; to: 1.0; duration: 500 }
    }

    ScrollBar {}

    FileDialog {
        id: fileDialog
        title: qsTr("Please choose a file")
        folder: shortcuts.documents
        modality: Qt.NonModal
        nameFilters: [ "GCode files (*.gc *.ngc *.nc)", "All files (*)" ]
        selectMultiple: true

        onAccepted: {
            if(appLogic.isIdle) jobManager.addJobs(fileDialog.fileUrls)
        }
    }
}

