import QtQuick 2.0

import StyleFile 1.0
import QtGraphicalEffects 1.0


Item {
    readonly property real sizeUnit: parent.width / 12
    property color activatedAreaColor: Style.controls.pressedColor
    property color directionTextColor: enabled ? Style.controls.textColor : Qt.darker(Style.controls.textColor)
    property bool enabled: grblBoard.isJogAllowed && appLogic.isIdle
    property vector3d axisValues: Qt.vector3d(0,0,0)

    id:jogControls

    Behavior on directionTextColor {
        ColorAnimation {duration: 50}
    }

    anchors.leftMargin: sizeUnit
    anchors.rightMargin: sizeUnit
    height: sizeUnit * 6.5

    JogPad {
        id:xypad

        radius: jogControls.sizeUnit / 4
        anchors.left: jogControls.left
        anchors.top: jogControls.top
        width: jogControls.sizeUnit * 6
        height : jogControls.sizeUnit * 6

        Rectangle {
            anchors.margins: parent.border.width
            height: jogControls.sizeUnit * 2 - parent.border.width * 2
            width: height
            radius:parent.radius
            anchors.top:parent.top
            anchors.left: parent.left
            color:jogControls.activatedAreaColor
            visible: Qt.vector2d(jogController.axisValues.x,
                                 jogController.axisValues.y) == Qt.vector2d(-1,1)
        }

        Rectangle {
            anchors.margins: parent.border.width
            height: jogControls.sizeUnit * 2 - parent.border.width * 2
            width: height
            radius:parent.radius
            anchors.top:parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            color:jogControls.activatedAreaColor
            visible: Qt.vector2d(jogController.axisValues.x,
                                 jogController.axisValues.y) == Qt.vector2d(0,1)
        }

        Rectangle {
            anchors.margins: parent.border.width
            height: jogControls.sizeUnit * 2 - parent.border.width * 2
            width: height
            radius:parent.radius
            anchors.top:parent.top
            anchors.right: parent.right
            color:jogControls.activatedAreaColor
            visible: Qt.vector2d(jogController.axisValues.x,
                                 jogController.axisValues.y) == Qt.vector2d(1,1)
        }

        Rectangle {
            anchors.margins: parent.border.width
            height: jogControls.sizeUnit * 2 - parent.border.width * 2
            width: height
            radius:parent.radius
            anchors.verticalCenter:parent.verticalCenter
            anchors.left: parent.left
            color:jogControls.activatedAreaColor
            visible: Qt.vector2d(jogController.axisValues.x,
                                 jogController.axisValues.y) == Qt.vector2d(-1,0)
        }

        Rectangle {
            anchors.margins: parent.border.width
            height: jogControls.sizeUnit * 2 - parent.border.width * 2
            width: height
            radius:parent.radius
            anchors.verticalCenter:parent.verticalCenter
            anchors.right: parent.right
            color:jogControls.activatedAreaColor
            visible: Qt.vector2d(jogController.axisValues.x,
                                 jogController.axisValues.y) == Qt.vector2d(1,0)
        }

        Rectangle {
            anchors.margins: parent.border.width
            height: jogControls.sizeUnit * 2 - parent.border.width * 2
            width: height
            radius:parent.radius
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            color:jogControls.activatedAreaColor
            visible: Qt.vector2d(jogController.axisValues.x,
                                 jogController.axisValues.y) == Qt.vector2d(-1,-1)
        }

        Rectangle {
            anchors.margins: parent.border.width
            height: jogControls.sizeUnit * 2 - parent.border.width * 2
            width: height
            radius:parent.radius
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            color:jogControls.activatedAreaColor
            visible: Qt.vector2d(jogController.axisValues.x,
                                 jogController.axisValues.y) == Qt.vector2d(0,-1)
        }

        Rectangle {
            anchors.margins: parent.border.width
            height: jogControls.sizeUnit * 2 - parent.border.width * 2
            width: height
            radius:parent.radius
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            color:jogControls.activatedAreaColor
            visible: Qt.vector2d(jogController.axisValues.x,
                                 jogController.axisValues.y) == Qt.vector2d(1,-1)
        }

        Text {
            text: "Y+"
            color: jogControls.directionTextColor
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            height: jogControls.sizeUnit*2
            width: jogControls.sizeUnit*2
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: jogControls.sizeUnit
            fontSizeMode: Text.Fit
        }

        Text {
            text: "Y-"
            color: jogControls.directionTextColor
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            height: jogControls.sizeUnit*2
            width: jogControls.sizeUnit*2
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: jogControls.sizeUnit
            fontSizeMode: Text.Fit
        }

        Text {
            text: "X-"
            color: jogControls.directionTextColor
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            height: jogControls.sizeUnit*2
            width: jogControls.sizeUnit*2
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: jogControls.sizeUnit
            fontSizeMode: Text.Fit
        }

        Text {
            text: "X+"
            color: jogControls.directionTextColor
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            height: jogControls.sizeUnit*2
            width: jogControls.sizeUnit*2
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: jogControls.sizeUnit
            fontSizeMode: Text.Fit
        }
    }

    Item {
        id: indicatorArea

        anchors.left: xypad.right
        anchors.right: zpad.left
        anchors.top: parent.top
        height: jogControls.sizeUnit * 6

        Rectangle {
            id: indicatorBg
            anchors.centerIn: parent
            width : parent.width / 3
            height: parent.height
            radius: width / 2
            color: Style.controls.bgColor
            visible: jogControls.enabled

            Rectangle {
                id: indicator
                anchors.centerIn: parent
                width : parent.width
                height: getHeight()
                radius: parent.radius
                color: Style.controls.indicatorColor
                function getHeight()
                {
                    return (jogController.speedRatio === 0.0) ? 0.0 :
                            width + (parent.height - width) * jogController.speedRatio
                }
            }
        }


        Image {
            id: lockedImage
            source: "qrc:/icons/controls/locked"
            visible: false
            anchors.centerIn: parent
            width: jogControls.sizeUnit * 1.5
            height: jogControls.sizeUnit * 1.5
            sourceSize.width: width
            sourceSize.height: height
        }

        ColorOverlay {
            id: lockedImageOverlay
            anchors.fill: lockedImage
            source: lockedImage
            color: Qt.darker(Style.controls.indicatorLockedIconColor)
            visible: !jogControls.enabled
        }
    }

    JogPad {
        id:zpad

        radius: jogControls.sizeUnit / 4
        anchors.right: jogControls.right
        anchors.top: jogControls.top
        width: jogControls.sizeUnit * 2
        height: jogControls.sizeUnit * 6

        Rectangle {
            anchors.margins: parent.border.width
            height: jogControls.sizeUnit * 2 - parent.border.width * 2
            width: height
            radius:parent.radius
            anchors.top:parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            color:jogControls.activatedAreaColor
            visible: jogController.axisValues.z === 1
        }

        Rectangle {
            anchors.margins: parent.border.width
            height: jogControls.sizeUnit * 2 - parent.border.width * 2
            width: height
            radius:parent.radius
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            color:jogControls.activatedAreaColor
            visible: jogController.axisValues.z === -1
        }

        Text {
            text: "Z+"
            color: jogControls.directionTextColor
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            height: jogControls.sizeUnit*2
            width: jogControls.sizeUnit*2
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: jogControls.sizeUnit
            fontSizeMode: Text.Fit
        }

        Text {
            text: "Z-"
            color: jogControls.directionTextColor
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            height: jogControls.sizeUnit*2
            width: jogControls.sizeUnit*2
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: jogControls.sizeUnit
            fontSizeMode: Text.Fit
        }
    }

    Item {
        id: jogController

        property vector3d graphicJogPadAxis: Qt.vector3d(xypad.axesValues.x,
                                                         xypad.axesValues.y,
                                                         zpad.axesValues.y)

        property vector3d keyboardJogPadAxis: jogControls.axisValues

        property vector3d axisValues

        Binding {
            target: jogController
            property: "axisValues"
            when: jogControls.enabled
            value: selectOneSource(jogController.graphicJogPadAxis, jogController.keyboardJogPadAxis)

            function selectOneSource(a, b) {
                if(a !== Qt.vector3d(0,0,0) && b === Qt.vector3d(0,0,0)) {
                    return a;
                }
                else if(a === Qt.vector3d(0,0,0) && b !== Qt.vector3d(0,0,0)) {
                    return b;
                }
                else {
                    return Qt.vector3d(0,0,0);
                }
            }
        }

        property double speedRatio: 0

        states: State {
            name: "go"
            PropertyChanges {
                target: jogController
                speedRatio:1.0
            }
        }

        state: (axisValues != Qt.vector3d(0,0,0)) ? "go" : ""

        transitions:Transition {
            from: ""; to: "go"
            PropertyAnimation {
                easing.type: Easing.Linear
                properties: "speedRatio"; duration: 5000 }
        }


        onAxisValuesChanged: sendJogCommand()
        onSpeedRatioChanged: sendJogCommand()

        function sendJogCommand(){
            var jogVector = axisValues.times(speedRatio)

            grblBoard.jog(jogVector)
        }

    }
}
