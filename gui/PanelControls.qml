import QtQuick 2.0

import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import GrblStatus 1.0
import GrblBoard 1.0

import StyleFile 1.0
Item {

    ScrollBar {
        target: flickableControlsPanel
    }

    Flickable {
        id: flickableControlsPanel
        anchors.fill: parent
        contentWidth: width
        contentHeight: contentColumn.height
        boundsBehavior: Flickable.StopAtBounds
        clip: true

        Column {
            x: 0
            id: contentColumn
            anchors.left: parent.left
            anchors.right: parent.right

            ItemGroup{
                id: jogGroup
                title: qsTr("Jog")

                contentHeight: jogControls.height

                JogControls {
                    id:jogControls
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.right: parent.right
                }

            }

            Rectangle {
                id: separator1
                height: 1
                width: parent.width * Style.separator.span
                color: Style.separator.color
                anchors.horizontalCenter: parent.horizontalCenter
            }

            ItemGroup {
                id:grblManualCommandsGroup
                title:qsTr("Manual Commands")
                contentHeight: width * 0.25

                CustomButton{
                    id:homingbutton

                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.right: parent.horizontalCenter

                    imageSource: "qrc:/icons/controls/Homing"

                    caption: qsTr("Run homing")

                    enabled: canRunHoming()

                    onClicked: grblBoard.homing()

                    function canRunHoming(){
                        return grblConfig.homingEnabled &&
                                ((grblStatus.currentState === GrblStatus.State_Alarm) ||
                                 (grblBoard.isIdle && appLogic.isIdle))

                    }
                }

                CustomButton{
                    id:killAlarmButton

                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: parent.horizontalCenter
                    anchors.right: parent.right

                    imageSource: "qrc:/icons/controls/KillAlarm"

                    caption: qsTr("Kill alarm")

                    enabled: (grblStatus.currentState === GrblStatus.State_Alarm)

                    onClicked: grblBoard.killAlarm()
                }
            }

            Rectangle {
                id: separator2
                height: 1
                width: parent.width * Style.separator.span
                color: Style.separator.color
                anchors.horizontalCenter: parent.horizontalCenter
            }

            ItemGroup {
                id: accessoryGroup
                title: qsTr("Overrides")
                contentHeight: overrideControlsColumn.height

                Column {
                    id: overrideControlsColumn

                    anchors.top:parent.top
                    anchors.left: parent.left
                    anchors.right: parent.right

                    enabled: (grblStatus.currentState !== GrblStatus.State_Offline) &&
                             (grblStatus.currentState !== GrblStatus.State_Unknown)

                    OverrideControlBox {
                        id: feedOverridesBox

                        anchors.left: parent.left
                        anchors.right: parent.right

                        label: qsTr("Feed motions")
                        value: grblStatus.overrideFeed

                        CustomButton {
                            imageSource: "qrc:/icons/overrides/slower2"
                            width:parent.width / 5
                            height: parent.height
                            onClicked: grblBoard.applyOverride(GrblBoard.Ovr_Feed_Dec)
                        }
                        CustomButton {
                            imageSource: "qrc:/icons/overrides/slower1"
                            width:parent.width / 5
                            height: parent.height
                            onClicked: grblBoard.applyOverride(GrblBoard.Ovr_Feed_Dec_Fine)
                        }
                        CustomButton {
                            imageSource: "qrc:/icons/overrides/reset"
                            width:parent.width / 5
                            height: parent.height
                            onClicked: grblBoard.applyOverride(GrblBoard.Ovr_Feed_Normal)
                        }
                        CustomButton {
                            imageSource: "qrc:/icons/overrides/faster1"
                            width:parent.width / 5
                            height: parent.height
                            onClicked: grblBoard.applyOverride(GrblBoard.Ovr_Feed_Inc_Fine)
                        }
                        CustomButton {
                            imageSource: "qrc:/icons/overrides/faster2"
                            width:parent.width / 5
                            height: parent.height
                            onClicked: grblBoard.applyOverride(GrblBoard.Ovr_Feed_Inc)
                        }
                    }

                    OverrideControlBox {
                        id: rapidOverridesBox

                        anchors.left: parent.left
                        anchors.right: parent.right

                        label: qsTr("Rapid motions")
                        value: grblStatus.overrideRapid

                        CustomButton {
                            imageSource: "qrc:/icons/overrides/slow2"
                            width:parent.width / 3
                            height: parent.height
                            onClicked: grblBoard.applyOverride(GrblBoard.Ovr_Rapid_Slower)
                        }
                        CustomButton {
                            imageSource: "qrc:/icons/overrides/slow1"
                            width:parent.width / 3
                            height: parent.height
                            onClicked: grblBoard.applyOverride(GrblBoard.Ovr_Rapid_Slow)
                        }
                        CustomButton {
                            imageSource: "qrc:/icons/overrides/reset"
                            width:parent.width / 3
                            height: parent.height
                            onClicked: grblBoard.applyOverride(GrblBoard.Ovr_Rapid_Normal)
                        }
                    }

                    OverrideControlBox {
                        id: spindleOverridesBox

                        anchors.left: parent.left
                        anchors.right: parent.right

                        label: qsTr("Spindle RPM")
                        value: grblStatus.overrideSpindle

                        CustomButton {
                            imageSource: "qrc:/icons/overrides/slower2"
                            width:parent.width / 5
                            height: parent.height
                            onClicked: grblBoard.applyOverride(GrblBoard.Ovr_Spindle_Dec)
                        }
                        CustomButton {
                            imageSource: "qrc:/icons/overrides/slower1"
                            width:parent.width / 5
                            height: parent.height
                            onClicked: grblBoard.applyOverride(GrblBoard.Ovr_Spindle_Dec_Fine)
                        }
                        CustomButton {
                            imageSource: "qrc:/icons/overrides/reset"
                            width:parent.width / 5
                            height: parent.height
                            onClicked: grblBoard.applyOverride(GrblBoard.Ovr_Spindle_Normal)
                        }
                        CustomButton {
                            imageSource: "qrc:/icons/overrides/faster1"
                            width:parent.width / 5
                            height: parent.height
                            onClicked: grblBoard.applyOverride(GrblBoard.Ovr_Spindle_Inc_Fine)
                        }
                        CustomButton {
                            imageSource: "qrc:/icons/overrides/faster2"
                            width:parent.width / 5
                            height: parent.height
                            onClicked: grblBoard.applyOverride(GrblBoard.Ovr_Spindle_Inc)
                        }
                    }
                }
            }
        }
    }

    // Control panel keyboard handling

    Keys.onPressed: {
        if(!event.isAutoRepeat && event.modifiers & Qt.KeypadModifier) {
            if (event.key === Qt.Key_4) {
                jogControls.axisValues.x = -1;
            }
            if (event.key === Qt.Key_6) {
                jogControls.axisValues.x = +1;
            }
            if (event.key === Qt.Key_8) {
                jogControls.axisValues.y = +1;
            }
            if (event.key === Qt.Key_2) {
                jogControls.axisValues.y = -1;
            }
            if (event.key === Qt.Key_9) {
                jogControls.axisValues.z = +1;
            }
            if (event.key === Qt.Key_3) {
                jogControls.axisValues.z = -1;
            }
        }
        if (event.key === Qt.Key_NumLock) {
            jogControls.axisValues.x = 0;
            jogControls.axisValues.y = 0;
            jogControls.axisValues.z = 0;
        }
    }

    Keys.onReleased: {
        if(!event.isAutoRepeat) {
            if (event.key === Qt.Key_4 || event.key == Qt.Key_6) {
                jogControls.axisValues.x = 0;
            }
            if (event.key === Qt.Key_8 || event.key == Qt.Key_2) {
                jogControls.axisValues.y = 0;
            }
            if (event.key === Qt.Key_9 || event.key == Qt.Key_3) {
                jogControls.axisValues.z = 0;
            }
            if(event.key === Qt.Key_7) {
                grblBoard.homing()
            }
        }
        if (event.key === Qt.Key_NumLock) {
            jogControls.axisValues.x = 0;
            jogControls.axisValues.y = 0;
            jogControls.axisValues.z = 0;
        }
    }

    // Prevent joging from continue when switching tabs avoiding lose of control
    onActiveFocusChanged: {
        if(activeFocus === false) {
            jogControls.axisValues.x = 0;
            jogControls.axisValues.y = 0;
            jogControls.axisValues.z = 0;
        }
    }
}
