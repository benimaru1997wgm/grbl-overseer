<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>CustomDialog</name>
    <message>
        <location filename="../CustomDialog.qml" line="212"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../CustomDialog.qml" line="289"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../CustomDialog.qml" line="303"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../CustomDialog.qml" line="317"/>
        <source>Abort</source>
        <translation>Abandon</translation>
    </message>
</context>
<context>
    <name>GRBLSettingsDelegate</name>
    <message>
        <location filename="../GRBLSettingsDelegate.qml" line="124"/>
        <location filename="../GRBLSettingsDelegate.qml" line="154"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../GRBLSettingsDelegate.qml" line="145"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../GRBLSettingsDelegate.qml" line="145"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
</context>
<context>
    <name>JobDelegate</name>
    <message>
        <location filename="../JobDelegate.qml" line="62"/>
        <source> lines</source>
        <translation> lignes</translation>
    </message>
    <message>
        <location filename="../JobDelegate.qml" line="156"/>
        <source>Set origin here</source>
        <translation>Déf. origine ici</translation>
    </message>
    <message>
        <location filename="../JobDelegate.qml" line="186"/>
        <source>Change color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../JobDelegate.qml" line="214"/>
        <source>Delete job</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>PanelControls</name>
    <message>
        <location filename="../PanelControls.qml" line="32"/>
        <source>Jog</source>
        <translation>Déplacements</translation>
    </message>
    <message>
        <location filename="../PanelControls.qml" line="55"/>
        <source>Manual Commands</source>
        <translation>Commandes manuelles</translation>
    </message>
    <message>
        <location filename="../PanelControls.qml" line="68"/>
        <source>Run homing</source>
        <translation>Origine machine</translation>
    </message>
    <message>
        <location filename="../PanelControls.qml" line="92"/>
        <source>Kill alarm</source>
        <translation>Réarmer</translation>
    </message>
    <message>
        <location filename="../PanelControls.qml" line="110"/>
        <source>Overrides</source>
        <translation>Modificateurs</translation>
    </message>
    <message>
        <location filename="../PanelControls.qml" line="126"/>
        <source>Feed motions</source>
        <translation>Mvts. effectifs</translation>
    </message>
    <message>
        <location filename="../PanelControls.qml" line="167"/>
        <source>Rapid motions</source>
        <translation>Mvts. rapides</translation>
    </message>
    <message>
        <location filename="../PanelControls.qml" line="196"/>
        <source>Spindle RPM</source>
        <translation>Rotation de l&apos;outil</translation>
    </message>
</context>
<context>
    <name>PanelJobs</name>
    <message>
        <location filename="../PanelJobs.qml" line="36"/>
        <source>Load more G-Code jobs</source>
        <translation>Charger plus de tâches G-Code</translation>
    </message>
    <message>
        <location filename="../PanelJobs.qml" line="36"/>
        <source>Load G-Code jobs</source>
        <translation>Charger une tâche G-Code</translation>
    </message>
    <message>
        <location filename="../PanelJobs.qml" line="73"/>
        <source>Please choose a file</source>
        <translation>Merci de choisir un fichier</translation>
    </message>
</context>
<context>
    <name>PanelMonitor</name>
    <message>
        <location filename="../PanelMonitor.qml" line="22"/>
        <source>Buffer status : </source>
        <translation>Etat de la mémoire tampon :</translation>
    </message>
    <message>
        <location filename="../PanelMonitor.qml" line="67"/>
        <source>Type manual commands here</source>
        <translation>Entrez les commandes manuelles ici</translation>
    </message>
</context>
<context>
    <name>PanelSettings</name>
    <message>
        <location filename="../PanelSettings.qml" line="13"/>
        <source>Serial Link</source>
        <translation>Port série</translation>
    </message>
    <message>
        <location filename="../PanelSettings.qml" line="66"/>
        <source>Disconnect</source>
        <translation>Déconnecter</translation>
    </message>
    <message>
        <location filename="../PanelSettings.qml" line="66"/>
        <source>Connect</source>
        <translation>Connecter</translation>
    </message>
    <message>
        <location filename="../PanelSettings.qml" line="89"/>
        <source>GRBL Configuration</source>
        <translation>Configuration de GRBL</translation>
    </message>
    <message>
        <location filename="../PanelSettings.qml" line="121"/>
        <source>Machine
configuration
not available</source>
        <translation>Configuration
machine
non disponnible</translation>
    </message>
</context>
<context>
    <name>TopBar</name>
    <message>
        <location filename="../TopBar.qml" line="107"/>
        <source> (in)</source>
        <translation>(po)</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="107"/>
        <source> (mm)</source>
        <translation>(mm)</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="108"/>
        <source>Work. </source>
        <translation>Travail.</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="108"/>
        <source>Mach. </source>
        <translation>Machine.</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="123"/>
        <source>Grbl State</source>
        <translation>Etat de GRBL</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="130"/>
        <source>Idle</source>
        <translation>Au repos</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="132"/>
        <source>Running</source>
        <translation>Actif</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="134"/>
        <source>Paused</source>
        <translation>En pause</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="136"/>
        <source>Jogging</source>
        <translation>Déplacement</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="138"/>
        <source>Door</source>
        <translation>Porte ouverte</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="140"/>
        <source>Homing</source>
        <translation>Retour origine</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="142"/>
        <source>Alarm</source>
        <translation>Arrêt d&apos;urgence</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="144"/>
        <source>Simulation</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="146"/>
        <source>Sleeping</source>
        <translation>En veille</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="148"/>
        <source>Status unknown</source>
        <translation>Etat inconnu</translation>
    </message>
    <message>
        <location filename="../TopBar.qml" line="150"/>
        <source>Not connected</source>
        <translation>Déconnecté</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="147"/>
        <location filename="../main.qml" line="213"/>
        <source>Jobs</source>
        <translation>Tâches</translation>
    </message>
    <message>
        <location filename="../main.qml" line="156"/>
        <location filename="../main.qml" line="224"/>
        <source>Controls</source>
        <translation>Contrôles</translation>
    </message>
    <message>
        <location filename="../main.qml" line="165"/>
        <location filename="../main.qml" line="235"/>
        <source>Monitor</source>
        <translation>Moniteur</translation>
    </message>
    <message>
        <location filename="../main.qml" line="174"/>
        <location filename="../main.qml" line="246"/>
        <source>Settings</source>
        <translation>Param.</translation>
    </message>
    <message>
        <location filename="../main.qml" line="276"/>
        <source>Go</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location filename="../main.qml" line="290"/>
        <source>Resume</source>
        <translation>Reprendre</translation>
    </message>
    <message>
        <location filename="../main.qml" line="303"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../main.qml" line="329"/>
        <source>Reset</source>
        <translation>Réinit.</translation>
    </message>
    <message>
        <location filename="../main.qml" line="341"/>
        <source>All jobs completed !</source>
        <translation>Toutes les tâches sont terminées !</translation>
    </message>
    <message>
        <location filename="../main.qml" line="349"/>
        <source>Simulation completed with errors</source>
        <translation>Simulation terminée avec des erreurs</translation>
    </message>
    <message>
        <location filename="../main.qml" line="350"/>
        <source>Start production anyway ?</source>
        <translation>Démarrer la production malgré tout ?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="351"/>
        <source>Errors :</source>
        <translation>Erreurs :</translation>
    </message>
    <message>
        <location filename="../main.qml" line="362"/>
        <source>Simulation completed without errors.</source>
        <translation>Simulation terminée sans erreurs.</translation>
    </message>
    <message>
        <location filename="../main.qml" line="363"/>
        <source>Start production ?</source>
        <translation>Démarrer la production ?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="374"/>
        <source>Command Error</source>
        <translation>Commande erronée</translation>
    </message>
    <message>
        <location filename="../main.qml" line="375"/>
        <source>Grbl just returned an error. Continue despite error ?</source>
        <translation>GRBL a retourné une erreur. Continuer malgré tout ?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="376"/>
        <source>Error message :</source>
        <translation>Message d&apos;erreur :</translation>
    </message>
    <message>
        <location filename="../main.qml" line="388"/>
        <source>Alarm</source>
        <translation>Arrêt d&apos;urgence</translation>
    </message>
    <message>
        <location filename="../main.qml" line="389"/>
        <source>Grbl is in Alarm state. Use Kill Alarm or Run homing commands in Controls tab to return to idle mode</source>
        <translation>L&apos;arrêt d&apos;urgence est activé. Utilisez la commande &quot;Réarmer&quot; ou &quot;Origine machine&quot; sur le panneau de contrôle manuel pour retourner à un état normal.</translation>
    </message>
    <message>
        <location filename="../main.qml" line="390"/>
        <source>Alarm message : </source>
        <translation>Message d&apos;alarme :</translation>
    </message>
    <message>
        <location filename="../main.qml" line="433"/>
        <source>Please wait</source>
        <translation>Veuillez patienter</translation>
    </message>
    <message>
        <location filename="../main.qml" line="434"/>
        <source>Performing homing sequence...</source>
        <translation>Retour à l&apos;origine machine...</translation>
    </message>
    <message>
        <location filename="../main.qml" line="445"/>
        <source>Please confirm action</source>
        <translation>Veuillez confirmer l&apos;action</translation>
    </message>
    <message>
        <location filename="../main.qml" line="446"/>
        <source>Do you really want to quit ?</source>
        <translation>Voulez-vous vraiment quitter ?</translation>
    </message>
    <message>
        <location filename="../main.qml" line="455"/>
        <source>Action restricted</source>
        <translation>Action impossible</translation>
    </message>
    <message>
        <location filename="../main.qml" line="456"/>
        <source>Can not quit while jobs are running</source>
        <translation>Impossible de quitter l&apos;application tant-qu&apos;une tâche est active</translation>
    </message>
    <message>
        <location filename="../main.qml" line="457"/>
        <source>Use &quot;Reset&quot; button to abort job</source>
        <translation>Utilisez le bouton &quot;Réinitialiser&quot; pour annuler la tâche en cours</translation>
    </message>
    <message>
        <location filename="../main.qml" line="463"/>
        <source>Unsupported GRBL version</source>
        <translation>Version de GRBL non supportée</translation>
    </message>
    <message>
        <location filename="../main.qml" line="464"/>
        <source>This GRBL version is not supported, only auxiliary features will be available.
 Please upgrade to Grbl 1.1 or later</source>
        <translation>Cette version de GRBL n&apos;est pas supportée, seul les fonctionnalités auxiliaires sont disponnibles.
Veuillez mettre à jour GRBL vers la version 1.1 ou ultérieure</translation>
    </message>
</context>
</TS>
