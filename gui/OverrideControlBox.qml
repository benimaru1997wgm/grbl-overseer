import QtQuick 2.0

import StyleFile 1.0

Item {
    id: overrideControlBox

    readonly property real sizeUnit: parent.width / 12

    property alias label: labelText.text
    property alias value: valueText.value
    default property alias controls : controlsRow.data

    height : sizeUnit * 3.5

    Rectangle {
        id: innerRectangle

        radius: sizeUnit / 4

        anchors.fill: parent

        anchors.leftMargin: sizeUnit
        anchors.rightMargin: sizeUnit
        anchors.bottomMargin: sizeUnit / 2
        anchors.topMargin: 0

        border.color: Style.controls.borderColor
        border.width: height * Style.controls.borderWidth
        color: Style.controls.bgColor

        Text {
            id: labelText

            anchors.top: parent.top
            anchors.bottom: controlsRow.top
            anchors.left: parent.left
            anchors.leftMargin:  sizeUnit / 4

            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft

            color: Style.controls.textColor
            font.bold: true
            font.pixelSize: overrideControlBox.width * Style.controls.overrideTextFontSizeFactor
        }

        Text {
            id: valueText

            property int value: 0

            text: value + " %"

            anchors.top: parent.top
            anchors.bottom: controlsRow.top
            anchors.right: parent.right
            anchors.rightMargin:  sizeUnit / 4

            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight

            color: Style.controls.textColor
            font.bold: true
            font.pixelSize: overrideControlBox.width * Style.controls.overrideTextFontSizeFactor
        }

        Row {
            id: controlsRow

            height: innerRectangle.height *  (1 - Style.controls.overrideTextAreaSizeFactor)

            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
        }
    }
}

