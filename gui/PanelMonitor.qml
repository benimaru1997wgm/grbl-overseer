import QtQuick 2.0

import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import GrblStatus 1.0

import StyleFile 1.0

Item {
    anchors.fill: parent

    Loader {
        id: bufferStatus
        visible: grblStatus.hasBufferStatus
        sourceComponent: visible ? bufferStatusComponent : null;
        height: visible ? width / 12 : 0

        anchors.top:parent.top
        anchors.left: parent.left
        anchors.right: parent.right

        property int planBufferDepth: grblBuild.planBufferDepth
        property int charBufferDepth: grblBuild.charBufferDepth
        property int planBufferUsed: grblBuild.planBufferDepth - grblStatus.planBufferFreeSlots
        property int charBufferUsed: grblBuild.charBufferDepth - grblStatus.charBufferFreeSlots

        Component {
            id:bufferStatusComponent

            Item{
                ProgressBar {                    
                    anchors.left: parent.left
                    anchors.right: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom

                    anchors.leftMargin: parent.width / 25
                    anchors.rightMargin: parent.width / 50
                    anchors.topMargin: parent.width / 100
                    anchors.bottomMargin: parent.width / 100

                    minimumValue: 0
                    maximumValue: planBufferDepth
                    value: planBufferUsed

                    Text{
                        id: planBufText

                        anchors.fill: parent

                        text: generateText()

                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        color: "black"

                        font.pixelSize: parent.height * 0.7

                        function generateText(){
                            if(grblStatus.planBufferFreeSlots < 0)
                                return qsTr("Plan Buf : Err")
                            else
                                return qsTr("Plan Buf : ") + planBufferUsed + " / " + planBufferDepth
                        }
                    }
                }

                ProgressBar {
                    anchors.left: parent.horizontalCenter
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom

                    anchors.leftMargin: parent.width / 50
                    anchors.rightMargin: parent.width / 25
                    anchors.topMargin: parent.width / 100
                    anchors.bottomMargin: parent.width / 100

                    minimumValue: 0
                    maximumValue: charBufferDepth
                    value: charBufferUsed

                    Text{
                        id: charBufText

                        anchors.fill: parent

                        text: generateText()

                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        color: "black"

                        font.pixelSize: parent.height * 0.7

                        function generateText(){
                            if(grblStatus.charBufferFreeSlots < 0)
                                return qsTr("Char Buf : Err")
                            else
                                return qsTr("Char Buf : ") + charBufferUsed + " / " + charBufferDepth
                        }
                    }
                }
            }
        }
    }



    ListView {
        id: historyList
        model: visible ? grblHistory : null
        clip:true
        boundsBehavior: Flickable.StopAtBounds
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: bufferStatus.bottom
        anchors.bottom: userCommandText.top

        delegate: HistoryDelegate {
            width: parent.width
        }

        onModelChanged: {
            currentIndex = -1
        }

        Connections {
            target: grblHistory
            onMessageAddedToItem:{
                if(historyList.atYEnd)
                {
                    historyList.currentIndex = index
                    historyList.positionViewAtEnd()
                }
            }
        }

        ScrollBar { }
    }

    TextField{
        id:userCommandText

        enabled: grblBoard.isIdle && appLogic.isIdle

        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        height: parent.width * Style.history.manualCommand.aspectRatio

        placeholderText: qsTr("Type manual commands here")

        style: TextFieldStyle {
            textColor: control.enabled ? Style.input.enabledColor : Style.input.disabledColor
            font.bold: true
            font.pixelSize: control.parent.width * Style.history.manualCommand.textPixelSizeRatio
            placeholderTextColor: control.enabled ? Style.input.enabledColor : Style.input.disabledColor
            background: Rectangle {
                color: Style.input.bgColor
            }
        }

        function clear()
        {
            userCommandText.text = ""
        }

        property string prevCommand: ""

        onAccepted: {
            //Only clear immediately if command can not be sent
            prevCommand = text
            if(!grblBoard.executeSingleInstruction(text)) clear()
        }

        onEnabledChanged: {
            //Clear when reenabling item
            if(enabled) clear()
        }

        Keys.onUpPressed: {
            userCommandText.text = prevCommand
        }
    }

    onActiveFocusChanged: {
        if (activeFocus === true) {
            userCommandText.forceActiveFocus();
        }
    }
}

