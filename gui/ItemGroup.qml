import QtQuick 2.0
import StyleFile 1.0

Column {
    default property alias content: content.data
    property alias title: title.text
    property alias contentHeight: content.height

    anchors.left: parent.left
    anchors.right: parent.right

    height: visible ? undefined : 0


    Text {
        id: title
        color: Style.groupTitle.color
        font.family: "Helvetica"
        font.bold: true
        font.pixelSize: Style.groupTitle.textSizeRatio * height
        height: width / Style.groupTitle.aspectRatio
        anchors.left: parent.left
        anchors.right: parent.right
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Item {
        id:content
        height: parent.height - title.height
        anchors.left: parent.left
        anchors.right: parent.right
    }
}
