import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import OpenGLVisualizerItem 1.0
import QtQuick.Window 2.2
import GrblStatus 1.0
import GrblBoard 1.0
import AppLogic 1.0

import StyleFile 1.0

Window {
    id: mainWindow
    objectName: "mainWindow"
    minimumWidth: 640
    minimumHeight: 480
    width: 640
    height: 480
    visible: true

    function limitPhysicalSize(wantedSize, maxPhysicalSize)
    {
        return Math.min(wantedSize,
                        Screen.pixelDensity * maxPhysicalSize)
    }

    TopBar {
        id: topBar
        anchors.top:parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: limitPhysicalSize(parent.height / 16.0, 15.0)
    }

    Item {
        id: mainPanel
        anchors.top: topBar.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        VisualizerItem {
            id:visualizer
            objectName: "visualizer"
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.left: sideBar.right

            Loader {
                active: (typeof(messageHandler)=="undefined") ? false : true //Only load if message handler exists
                anchors.fill: parent
                sourceComponent: debugTextAreaComponent

                Component {
                    id: debugTextAreaComponent
                    TextEdit {
                        id: debugTextArea
                        anchors.fill: parent
                        color: Style.text.defaultColor
                        text: "debug"
                        enabled: false // to disable mouse / kb / focus

                        Connections {
                            target: messageHandler
                            onNewMessage:
                            {
                                debugTextArea.insert(0,message)
                            }
                        }
                    }
                }
            }
        }


        SidePanel {
            id: sidePanel
            color: Style.sidePanel.bgColor
            anchors.top: sideBar.top
            anchors.bottom: sideBar.bottom
            anchors.left: sideBar.right
            clip: true

            //On vertical screen orientatio, occupy the whole screen
            deployedWidth: (Screen.primaryOrientation === Qt.PortraitOrientation)
                           ? visualizer.width
                           : parent.width / 3

            retractedWidth: 0
            state: tabs.currentIndex < 0 ? "retracted" : "deployed"

            MouseArea {
                anchors.fill: parent    //To capture events
                acceptedButtons: Qt.AllButtons
                preventStealing: true
                propagateComposedEvents: false

                onWheel: {
                    wheel.accepted = true
                }
            }

            Rectangle {
                id: titleRectangle
                anchors.top : parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                height: sideBar.width / 2
                color: Style.sidePanel.title.bgColor

                Text {
                    anchors.margins: parent.height / 20
                    anchors.fill: parent
                    text : titleOfCurrentTab()
                    color: Style.sidePanel.title.textColor
                    fontSizeMode: Text.Fit
                    minimumPointSize: 2
                    font.pointSize: 100
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter

                    function titleOfCurrentTab(){
                        var currentTab = tabs.getTab(tabs.currentIndex);
                        return currentTab ? currentTab.title : ""
                    }
                }
            }

            TabView {
                id: tabs
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top:titleRectangle.bottom
                anchors.bottom: parent.bottom
                tabsVisible: false
                currentIndex: -1

                style: TabViewStyle {
                    frameOverlap: 1
                    tabsAlignment: Qt.AlignHCenter
                    frame: Rectangle { color: sidePanel.color }
                }

                Tab {
                    id: tabJobs
                    title: qsTr("Jobs")
                    active:true //Force loading tab on startup
                    PanelJobs {
                        id: panelJobs
                    }
                }

                Tab {
                    id: tabControls
                    title: qsTr("Controls")
                    active:true //Force loading tab on startup
                    PanelControls{
                        id: panelControls
                    }
                }

                Tab {
                    id: tabMonitor
                    title: qsTr("Monitor")
                    active:true //Force loading tab on startup
                    PanelMonitor{
                        id: panelMonitor
                    }
                }

                Tab {
                    id: tabSettings
                    title: qsTr("Settings")
                    active:true //Force loading tab on startup
                    PanelSettings{
                        id: panelSettings
                    }
                }
            }
        }

        Rectangle {
            id: sideBar
            color: Style.sideBar.bgColor
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: limitPhysicalSize(parent.height / 8.0, 30.0)

            function isTabOpen(tabNum) {
                return tabs.currentIndex == tabNum
            }

            function toggleTab(tabNum)
            {
                tabs.currentIndex = (tabs.currentIndex == tabNum) ? -1 : tabNum
                if(tabs.currentIndex >= 0) {
                    tabs.getTab(tabs.currentIndex).item.forceActiveFocus();
                }
                else {
                    forceActiveFocus();
                }
            }

            Column {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top

                CustomButton {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    imageSource: "qrc:/icons/tab/Jobs"
                    engaged: sideBar.isTabOpen(0)
                    onClicked: sideBar.toggleTab(0)
                    overlayColorPressed: Style.sideBar.buttonColor.jobs
                    backgroundColorEngaged: Style.sideBar.buttonColor.engaged
                    caption: qsTr("Jobs")
                }

                CustomButton {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    imageSource:  "qrc:/icons/tab/Controls"
                    engaged: sideBar.isTabOpen(1)
                    onClicked: sideBar.toggleTab(1)
                    overlayColorPressed: Style.sideBar.buttonColor.controls
                    backgroundColorEngaged: Style.sideBar.buttonColor.engaged
                    caption: qsTr("Controls")
                }

                CustomButton {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    imageSource:  "qrc:/icons/tab/Monitor"
                    engaged: sideBar.isTabOpen(2)
                    onClicked: sideBar.toggleTab(2)
                    overlayColorPressed: Style.sideBar.buttonColor.monitor
                    backgroundColorEngaged: Style.sideBar.buttonColor.engaged
                    caption: qsTr("Monitor")
                }

                CustomButton {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    imageSource: "qrc:/icons/tab/Settings"
                    engaged: sideBar.isTabOpen(3)
                    onClicked: sideBar.toggleTab(3)
                    overlayColorPressed: Style.sideBar.buttonColor.settings;
                    backgroundColorEngaged: Style.sideBar.buttonColor.engaged
                    caption: qsTr("Settings")
                }

                Action {
                    shortcut: "F1"
                    onTriggered: sideBar.toggleTab(0)
                }
                Action {
                    shortcut: "F2"
                    onTriggered: sideBar.toggleTab(1)
                }
                Action {
                    shortcut: "F3"
                    onTriggered: sideBar.toggleTab(2)
                }
                Action {
                    shortcut: "F4"
                    onTriggered: sideBar.toggleTab(3)
                }
            }

            Column {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom

                TabView {
                    id:playPauseButton
                    width: parent.width
                    height:width
                    tabsVisible: false
                    currentIndex: getProperButtonIndex();

                    style: TabViewStyle {
                        frameOverlap: 1
                        tabsAlignment: Qt.AlignHCenter
                        frame: Rectangle { color: "transparent" }
                    }


                    Tab{
                        CustomButton {
                            anchors.left: parent.left
                            anchors.right: parent.right
                            imageSource:  "qrc:/icons/cmdPlay"
                            overlayColorIdle: Style.sideBar.buttonColor.start
                            overlayColorDisabled: Style.sideBar.buttonColor.disabled
                            caption: qsTr("Go")
                            enabled: appLogic.readyToGo
                            onClicked: {
                                appLogic.startSimulation();
                            }
                        }
                    }

                    Tab{
                        CustomButton {
                            anchors.left: parent.left
                            anchors.right: parent.right
                            imageSource:  "qrc:/icons/cmdPlay"
                            overlayColorIdle: Style.sideBar.buttonColor.resume
                            caption: qsTr("Resume")
                            onClicked: {
                                grblBoard.resume();
                            }
                        }
                    }

                    Tab {
                        CustomButton {
                            anchors.left: parent.left
                            anchors.right: parent.right
                            imageSource:  "qrc:/icons/cmdPause"
                            overlayColorIdle: Style.sideBar.buttonColor.idle
                            caption: qsTr("Pause")
                            onClicked: {
                                grblBoard.hold();
                            }
                        }
                    }

                    function getProperButtonIndex(){
                        if(grblStatus.currentState === GrblStatus.State_Hold || grblStatus.currentState === GrblStatus.State_Door)
                            return 1
                        //We also need to test if running, because if grbl buffer is starved, it will show in IDLE state
                        else if(grblStatus.currentState === GrblStatus.State_Run || appLogic.currentState === AppLogic.State_Production_Running)
                            return 2
                        else
                            return 0
                    }
                }

                CustomButton {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    imageSource: "qrc:/icons/cmdReset"
                    overlayColorIdle: Style.sideBar.buttonColor.reset
                    overlayColorDisabled: Style.sideBar.buttonColor.disabled

                    enabled: (grblStatus.currentState !== GrblStatus.State_Offline)
                    caption: qsTr("Reset")
                    onClicked: {
                        appLogic.abort();
                    }
                }
            }
        }
    }


    CustomDialog {
        id: errorOpeningJobDialog
        type:typeCritical
        title: qsTr("Error opening job")
        text: qsTr("An error was encountered while trying to open a job")
        informativeText: qsTr("Error message :")
        buttons: buttonOk
    }

    Connections{
        target: jobManager
        onJobLoadFailed:
        {
            errorOpeningJobDialog.detailedText = error
            errorOpeningJobDialog.open()
        }
    }

    CustomDialog {
        id: productionCompletedDialog
        type: typeInfo
        title: qsTr("All jobs completed !")
        onAccepted: appLogic.abort()
    }

    CustomDialog {
        id: simulationCompletedWithErrorsDialog
        type:typeWarning
        buttons: buttonYes | buttonNo
        title: qsTr("Simulation completed with errors")
        text: qsTr("Start production anyway ?")
        informativeText: qsTr("Errors :")
        onYes: appLogic.startProduction()
        onAccepted: appLogic.startProduction()

        onNo: appLogic.abort()
    }

    CustomDialog {
        id: simulationCompletedWithoutErrorsDialog
        type: typeInfo
        buttons: buttonYes | buttonNo
        title: qsTr("Simulation completed without errors.")
        text: qsTr("Start production ?")

        onYes: appLogic.startProduction()
        onAccepted: appLogic.startProduction()

        onNo: appLogic.abort()
    }

    CustomDialog {
        id: errorDuringProductionDialog
        type:typeCritical
        title: qsTr("Command Error")
        text: qsTr("Grbl just returned an error. Continue despite error ?")
        informativeText: qsTr("Error message :")
        buttons: buttonYes | buttonNo

        onYes: grblBoard.resume()
        onAccepted: grblBoard.resume()

        onNo: appLogic.abort()
    }

    CustomDialog {
        id: alarmDialog
        type:typeCritical
        title: qsTr("Alarm")
        text: qsTr("Grbl is in Alarm state. Use Kill Alarm or Run homing commands in Controls tab to return to idle mode")
        informativeText: qsTr("Alarm message : ")
        onAccepted: appLogic.abort()
    }

    Connections{
        target: appLogic

        onProductionCompleted: {
            productionCompletedDialog.open()
        }

        onProductionError: {
            errorDuringProductionDialog.detailedText = errorMsg
            errorDuringProductionDialog.open()
        }

        onSimulationCompleted: {
            if(appLogic.getErrorCount() === 0)
            {
                simulationCompletedWithoutErrorsDialog.open()
            }
            else
            {
                simulationCompletedWithErrorsDialog.detailedText = appLogic.getRegisteredErrors()
                simulationCompletedWithErrorsDialog.open()
            }
        }
    }

    Connections{
        target: grblBoard
        onAlarm: {
            alarmDialog.detailedText = alarmMsg
            alarmDialog.open()
        }
        onBoardVersionNotSupported:{
            boardVersionNotSupportedDialog.open()
        }
    }

    CustomDialog {
        id:homingDialog
        type: typeWarning
        title: qsTr("Please wait")
        text: qsTr("Performing homing sequence...")
        buttons: buttonAbort

        visible: grblBoard.homingState === GrblBoard.Homing_Running

        onRejected: appLogic.abort()
    }

    CustomDialog {
        id: closeWhenIdleDialog
        type: typeWarning
        title: qsTr("Please confirm action")
        text: qsTr("Do you really want to quit ?")
        buttons: buttonYes | buttonNo
        onYes: Qt.quit()
        onAccepted: Qt.quit()
    }

    CustomDialog {
        id: closeWhenRunningDialog
        type: typeCritical
        title: qsTr("Action restricted")
        text: qsTr("Can not quit while jobs are running")
        informativeText: qsTr("Use \"Reset\" button to abort job")
    }

    CustomDialog {
        id: boardVersionNotSupportedDialog
        type: typeWarning
        title: qsTr("Unsupported GRBL version")
        text: qsTr("This GRBL version is not supported, only auxiliary features will be available.\n Please upgrade to Grbl 1.1 or later")
    }

    onClosing: {
        close.accepted = false
        appLogic.isIdle ? closeWhenIdleDialog.open() : closeWhenRunningDialog.open()
    }
    Action {
        shortcut: "F11"
        onTriggered:
            mainWindow.visibility === Window.FullScreen
                     ? mainWindow.visibility = Window.Windowed
                     : mainWindow.visibility = Window.FullScreen
    }
    Action {
        shortcut: "F12"
        onTriggered: mainWindow.close()
    }
}

