import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import GrblStatus 1.0

import StyleFile 1.0

DeployableItem {

    extendedHeight: description.contentHeight + 8
    mainHeight: width * Style.settings.mainHeightFactor

    mainItem: Rectangle {
        anchors.fill: parent
        color: "transparent"

        Rectangle {
            id: indexSection

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.bottom: parent.bottom

            width: parent.width * Style.settings.indexSection.widthFactor

            color: "transparent"

            Text {
                id: index
                text: model.key
                color: Style.text.defaultColor
                font.family: "Helvetica"
                font.pixelSize: parent.parent.width * Style.settings.indexSection.textPixelSizeFactor
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                elide: Text.ElideRight
            }
        }

        Rectangle {
            id: labelSection

            color: "transparent"

            anchors.top: parent.top
            anchors.left: indexSection.right
            anchors.right: valueSection.left
            anchors.bottom: parent.bottom

            Text {
                id: label
                text: model.label
                color: Style.text.defaultColor
                font.family: "Helvetica"
                font.pixelSize: parent.parent.width * Style.settings.labelSection.textPixelSizeFactor
                font.bold: true
                anchors.fill: parent
                anchors.leftMargin:     parent.parent.width * Style.settings.labelSection.marginsFactor
                anchors.rightMargin:    parent.parent.width * Style.settings.labelSection.marginsFactor
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                elide: Text.ElideRight
                wrapMode: Text.WordWrap
            }
        }

        Rectangle {
            id: valueSection

            color: "transparent"

            width: parent.width * Style.settings.valueSection.widthFactor

            anchors.top: parent.top
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            Loader {
                id: valueEditor

                anchors.top: parent.top
                anchors.bottom: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: parent.left

                enabled: ((grblStatus.currentState === GrblStatus.State_Idle) ||
                          (grblStatus.currentState == GrblStatus.State_Alarm))
                         && appLogic.isIdle

                sourceComponent: selectLoaderForUnit(model.unit)

                property variant settingModel: model

                function selectLoaderForUnit(unit)
                {
                    switch(unit)
                    {
                    case "boolean":
                        return valueEditorBoolean
                    case "mask":
                        return valueEditorMask   // <- Change when mask editor is completed
                    default:
                        return valueEditorNumeric
                    }
                }
            }

            Component {
                id:valueEditorNumeric
                TextField {
                    text: settingModel.value
                    inputMethodHints: Qt.ImhDigitsOnly
                    validator: DoubleValidator {
                        bottom: 0
                        notation: DoubleValidator.StandardNotation
                        locale: "en"
                    }

                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    placeholderText: qsTr("Value")
                    style: TextFieldStyle {
                        textColor: control.enabled ? Style.input.enabledColor : Style.input.disabledColor
                        font.bold: true
                        font.pixelSize: valueSection.parent.width * Style.settings.valueSection.textPixelSizeFactor
                        placeholderTextColor: Style.input.placeholderColor
                        background: Rectangle {
                            color: Style.input.bgColor
                        }
                    }

                    onAccepted: {
                        settingModel.value = text
                        focus = false
                    }
                }
            }

            Component {
                id:valueEditorBoolean
                TextField {
                    text: (settingModel.value === 0.0) ? qsTr("No") : qsTr("Yes")
                    inputMethodHints: Qt.ImhDigitsOnly
                    validator: DoubleValidator {
                        bottom: 0
                        notation: DoubleValidator.StandardNotation
                        locale: "en"
                    }
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    placeholderText: qsTr("Value")
                    readOnly: true
                    style: TextFieldStyle {
                        textColor: control.enabled ? Style.input.enabledColor : Style.input.disabledColor
                        font.bold: true
                        font.pixelSize: valueSection.parent.width * Style.settings.valueSection.textPixelSizeFactor
                        placeholderTextColor: Style.input.placeholderColor
                        background: Rectangle {
                            color: Style.input.bgColor
                        }
                    }

                    onAccepted: {
                        settingModel.value = valueEditor.text
                        valueEditor.focus = false
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            settingModel.value = (settingModel.value === 0.0) ? 1 : 0
                        }
                    }
                }
            }

            Component {
                id:valueEditorMask

                Row {
                    property real bitMargins: width * Style.settings.valueSection.maskMarginsFactor

                    id: bitRow
                    spacing: bitMargins
                    layoutDirection: Qt.LeftToRight

                    Repeater {
                        model: settingModel.bits

                        Item {
                            width: parent.width / 3 - bitMargins
                            height: parent.height

                            readonly property int currBit: Math.pow(2, index)

                            Rectangle {
                                id: bitLetterPart

                                anchors.top: parent.top
                                anchors.bottom: indicatorPart.top
                                anchors.left: parent.left
                                anchors.right: parent.right

                                anchors.bottomMargin: bitMargins

                                color: Style.input.bgColor

                                Text {
                                    color: enabled ? Style.input.enabledColor : Style.input.disabledColor
                                    anchors.fill: parent
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                    text: modelData
                                    font.pixelSize: valueSection.parent.width * Style.settings.valueSection.textPixelSizeFactor
                                    font.bold: true
                                }
                            }

                            Rectangle {
                                id: indicatorPart
                                anchors.left: parent.left
                                anchors.right: parent.right
                                anchors.bottom: parent.bottom

                                height: parent.height * Style.settings.valueSection.maskIndicatorHeightFactor

                                color: getColor()

                                function getColor(){
                                    return (settingModel.value & currBit )
                                          ? ( enabled ? Style.input.enabledColor : Style.input.disabledColor)
                                          : Style.input.bgColor
                                }
                            }

                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    settingModel.value ^= currBit;
                                    //console.log("Clicked " + index  + " bitmask "+ getCurrBit() + " value " + modelData.value)
                                }
                            }
                        }
                    }
                }
            }

            Text {
                id: unit
                text: model.unit
                color: Style.text.defaultColor
                font.family: "Helvetica"
                font.pixelSize: valueSection.parent.width * Style.settings.valueSection.textPixelSizeFactor
                anchors.top: valueEditor.bottom
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.left: parent.left
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                elide: Text.ElideRight
            }
        }
    }

    extendedItem: Text {
        id: description
        text: model.description
        color: Style.text.defaultColor
        font.family: "Helvetica"
        font.pixelSize:         parent.width * Style.settings.description.textPixelSizeFactor

        anchors.fill: parent
        anchors.leftMargin:     parent.width * Style.settings.description.marginsFactor
        anchors.rightMargin:    parent.width * Style.settings.description.marginsFactor

        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideNone
        wrapMode: Text.WordWrap
    }

}
