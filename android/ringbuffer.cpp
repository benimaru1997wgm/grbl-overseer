#include "ringbuffer.h"

RingBuffer::RingBuffer(int growth) :
    m_head(0),
    m_tail(0),
    m_tailBuffer(0),
    m_basicBlockSize(growth),
    m_bufferSize(0)
{
    m_buffers.append(QByteArray());
}

int RingBuffer::nextDataBlockSize() const {
    return (m_tailBuffer == 0 ? m_tail : m_buffers.first().size()) - m_head;
}

const char *RingBuffer::readPointer() const {
    return m_bufferSize == 0 ? Q_NULLPTR : (m_buffers.first().constData() + m_head);
}

const char *RingBuffer::readPointerAtPosition(qint64 pos, qint64 &length) const {
    if (pos >= 0) {
        pos += m_head;
        for (int i = 0; i < m_buffers.size(); ++i) {
            length = (i == m_tailBuffer ? m_tail : m_buffers[i].size());
            if (length > pos) {
                length -= pos;
                return m_buffers[i].constData() + pos;
            }
            pos -= length;
        }
    }

    length = 0;
    return 0;
}

void RingBuffer::free(int bytes) {
    while (bytes > 0) {
        int blockSize = m_buffers.first().size() - m_head;

        if (m_tailBuffer == 0 || blockSize > bytes) {
            // keep a single block around if it does not exceed
            // the basic block size, to avoid repeated allocations
            // between uses of the buffer
            if (m_bufferSize <= bytes) {
                if (m_buffers.first().size() <= m_basicBlockSize) {
                    m_bufferSize = 0;
                    m_head = m_tail = 0;
                } else {
                    clear(); // try to minify/squeeze us
                }
            } else {
                m_head += bytes;
                m_bufferSize -= bytes;
            }
            return;
        }

        m_bufferSize -= blockSize;
        bytes -= blockSize;
        m_buffers.removeFirst();
        --m_tailBuffer;
        m_head = 0;
    }
}

char *RingBuffer::reserve(int bytes) {
    if (bytes <= 0)
        return 0;

    // if need buffer reallocation
    if (m_tail + bytes > m_buffers.last().size()) {
        if (m_tail + bytes > m_buffers.last().capacity() && m_tail >= m_basicBlockSize) {
            // shrink this buffer to its current size
            m_buffers.last().resize(m_tail);

            // create a new QByteArray
            m_buffers.append(QByteArray());
            ++m_tailBuffer;
            m_tail = 0;
        }
        m_buffers.last().resize(qMax(m_basicBlockSize, m_tail + bytes));
    }

    char *writePtr = m_buffers.last().data() + m_tail;
    m_bufferSize += bytes;
    m_tail += bytes;
    return writePtr;
}

void RingBuffer::truncate(int pos) {
    if (pos < size())
        chop(size() - pos);
}

void RingBuffer::chop(int bytes) {
    while (bytes > 0) {
        if (m_tailBuffer == 0 || m_tail > bytes) {
            // keep a single block around if it does not exceed
            // the basic block size, to avoid repeated allocations
            // between uses of the buffer
            if (m_bufferSize <= bytes) {
                if (m_buffers.first().size() <= m_basicBlockSize) {
                    m_bufferSize = 0;
                    m_head = m_tail = 0;
                } else {
                    clear(); // try to minify/squeeze us
                }
            } else {
                m_tail -= bytes;
                m_bufferSize -= bytes;
            }
            return;
        }

        m_bufferSize -= m_tail;
        bytes -= m_tail;
        m_buffers.removeLast();
        --m_tailBuffer;
        m_tail = m_buffers.last().size();
    }
}

bool RingBuffer::isEmpty() const {
    return m_bufferSize == 0;
}

int RingBuffer::getChar() {
    if (isEmpty())
        return -1;
    char c = *readPointer();
    free(1);
    return int(uchar(c));
}

void RingBuffer::putChar(char c) {
    char *ptr = reserve(1);
    *ptr = c;
}

void RingBuffer::ungetChar(char c) {
    --m_head;
    if (m_head < 0) {
        if (m_bufferSize != 0) {
            m_buffers.prepend(QByteArray());
            ++m_tailBuffer;
        } else {
            m_tail = m_basicBlockSize;
        }
        m_buffers.first().resize(m_basicBlockSize);
        m_head = m_basicBlockSize - 1;
    }
    m_buffers.first()[m_head] = c;
    ++m_bufferSize;
}

int RingBuffer::size() const {
    return m_bufferSize;
}

void RingBuffer::clear() {
    m_buffers.erase(m_buffers.begin() + 1, m_buffers.end());
    m_buffers.first().clear();

    m_head = m_tail = 0;
    m_tailBuffer = 0;
    m_bufferSize = 0;
}

int RingBuffer::indexOf(char c) const {
    int index = 0;
    int j = m_head;
    for (int i = 0; i < m_buffers.size(); ++i) {
        const char *ptr = m_buffers[i].constData() + j;
        j = index + (i == m_tailBuffer ? m_tail : m_buffers[i].size()) - j;

        while (index < j) {
            if (*ptr++ == c)
                return index;
            ++index;
        }
        j = 0;
    }
    return -1;
}

int RingBuffer::indexOf(char c, int maxLength) const {
    int index = 0;
    int j = m_head;
    for (int i = 0; index < maxLength && i < m_buffers.size(); ++i) {
        const char *ptr = m_buffers[i].constData() + j;
        j = qMin(index + (i == m_tailBuffer ? m_tail : m_buffers[i].size()) - j, maxLength);

        while (index < j) {
            if (*ptr++ == c)
                return index;
            ++index;
        }
        j = 0;
    }
    return -1;
}

int RingBuffer::read(char *data, int maxLength) {
    int bytesToRead = qMin(size(), maxLength);
    int readSoFar = 0;
    while (readSoFar < bytesToRead) {
        int bytesToReadFromThisBlock = qMin(bytesToRead - readSoFar, nextDataBlockSize());
        if (data)
            memcpy(data + readSoFar, readPointer(), bytesToReadFromThisBlock);
        readSoFar += bytesToReadFromThisBlock;
        free(bytesToReadFromThisBlock);
    }
    return readSoFar;
}

QByteArray RingBuffer::read() {
    if (m_bufferSize == 0)
        return QByteArray();

    QByteArray qba(m_buffers.takeFirst());

    qba.reserve(0); // avoid that resizing needlessly reallocates
    if (m_tailBuffer == 0) {
        qba.resize(m_tail);
        m_tail = 0;
        m_buffers.append(QByteArray());
    } else {
        --m_tailBuffer;
    }
    qba.remove(0, m_head); // does nothing if head is 0
    m_head = 0;
    m_bufferSize -= qba.size();
    return qba;
}

void RingBuffer::append(const QByteArray &qba) {
    if (m_tail == 0) {
        m_buffers.last() = qba;
    } else {
        m_buffers.last().resize(m_tail);
        m_buffers.append(qba);
        ++m_tailBuffer;
    }
    m_tail = qba.size();
    m_bufferSize += m_tail;
}

int RingBuffer::skip(int length) {
    return read(0, length);
}

int RingBuffer::readLine(char *data, int maxLength) {
    if (!data || --maxLength <= 0)
        return -1;

    int i = indexOf('\n', maxLength);
    i = read(data, i >= 0 ? (i + 1) : maxLength);

    // Terminate it.
    data[i] = '\0';
    return i;
}

bool RingBuffer::canReadLine() const {
    return indexOf('\n') >= 0;
}

