#ifndef KEEPAWAKEHELPER_H
#define KEEPAWAKEHELPER_H

// Code from https://stackoverflow.com/questions/34179653/how-do-i-prevent-an-android-device-from-going-to-sleep-from-qt-application#34179654

#include <QAndroidJniObject>

class KeepAwakeHelper
{
public:
    KeepAwakeHelper();
    virtual ~KeepAwakeHelper();

private:
    QAndroidJniObject m_wakeLock;
};

#endif // KEEPAWAKEHELPER_H
