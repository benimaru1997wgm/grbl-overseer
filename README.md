## OVERVIEW
Grbl Overseer is a multiplatform control software for the popular Arduino-based CNC firmware GRBL.  
It provides a simple, touch-friendly user interface, well suited for use on a tablet.

Main features : 
* Simple, easy to use, touch-friendly user interface
* 3D view of jobs, jobs dimensions, machine space and current tool position
* Schedule multiple jobs, each has its specified origin
* Automatically executes a simulation run before production, and compiles all errors
* Adaptative jog controls (the longer you press, the faster it goes)
* Overrides controls
* Smart serial console, GRBL message / responses are grouped with the corresponding command
* Smart top bar, always showing the current GRBL status. Background color changes with status to allow easy state reading even far from the device
* Built-in editor for GRBL configuration
* Multiplatform (tested on Windows, Linux and Android)
* Support USB / serial interface on > Android 3.1 devices with USB API
* Support Grbl >= v1.1

Developement is still ongoing, please report any issue you encounter

## SCREENSHOTS
"Jobs" panel deployed :

![](doc/screenshot_jobs_panel.png)

"Controls" panel deployed :

![](doc/screenshot_controls_panel.png)

"Monitor" panel deployed :

![](doc/screenshot_monitor_panel.png)

"Settings" panel deployed :

![](doc/screenshot_settings_panel.png)


"Alarm" dialog :

![](doc/screenshot_alarm_dialog.png)

## INSTALLATION
### Linux (Ubuntu / Mint)

    sudo apt-get update
    sudo apt-get install build-essential qt5-default libqt5serialport5-dev qtdeclarative5-dev qtdeclarative5-qtquick2-plugin qtdeclarative5-dialogs-plugin qtdeclarative5-controls-plugin libqt5qml-graphicaleffects qml-module-qt-labs-settings   qml-module-qt-labs-folderlistmodel qtdeclarative5-qtmultimedia-plugin
    qmake
    make
    ./grbl-overseer
    
### MS Windows
Archive with executable available here :
https://sourceforge.net/projects/grbl-overseer/


## MATERIAL SOURCES
Link Status Icons by FatCow
Instruction status icons by Ivan Boyko

USB Support inspired from Mike Goza qtserialport patch, based on usb-serial-for-android project

Icons Materiel Design by Google :  
https://github.com/google/material-design-icons/releases/tag/1.0.0  
  
Link Status Icons by FatCow  
Instruction status icons by Ivan Boyko  

Turtle / Snail icons from https://icons8.com

Sounds from SoundBible : 
http://soundbible.com/1540-Computer-Error-Alert.html
http://soundbible.com/1598-Electronic-Chime.html
