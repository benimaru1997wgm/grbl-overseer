#ifndef LINKMANAGER_H
#define LINKMANAGER_H

#include <QObject>
#include <QTimer>
#include <QMap>

class QIODevice;
class GrblBoard;

class LinkManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList portList READ getPortList NOTIFY portListUpdated)
    Q_PROPERTY(QStringList standardBaudratesList READ getStandardBaudratesList NOTIFY standardBaudRateListUpdated)
    Q_PROPERTY(bool linkOpen READ isLinkOpen NOTIFY linkOpenChanged)

public:
    explicit LinkManager(QObject *parent = 0);

    bool isLinkOpen();

    QStringList getPortList() const
        {return m_availablePortList;}

    QStringList getStandardBaudratesList() const
        {return QStringList({"115200", "9600"});}

signals:
    void portListUpdated();
    void standardBaudRateListUpdated();
    void linkOpenChanged(bool linkState);

    void linkDeviceChanged(QIODevice* linkDevice);

public slots:
    void openLink(int portIndex, qint32 baudRate);
    void closeLink();

private slots:
    void rescanAvailablePorts();

private:
    enum PortType{TypeQSerial};

    QTimer *m_scanTimer;
    QMap<PortType, QStringList> m_availablePorts;
    QStringList m_availablePortList;

    QIODevice* m_linkDevice;
};

#endif // LINKMANAGER_H
