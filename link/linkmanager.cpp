#include "linkmanager.h"

#ifdef Q_OS_ANDROID
#include <android/androidusbbridgeserialport.h>
#else
#include <QSerialPort>
#include <QSerialPortInfo>
#endif

#include <QDebug>

#define SCAN_INTERVAL   2000

LinkManager::LinkManager(QObject *parent) :
    QObject(parent),
    m_linkDevice(nullptr)
{
    m_scanTimer = new QTimer(this);
    m_scanTimer->start(SCAN_INTERVAL);
    connect(m_scanTimer, &QTimer::timeout, this, &LinkManager::rescanAvailablePorts);
}

bool LinkManager::isLinkOpen()
{
    return m_linkDevice && m_linkDevice->isOpen();
}


void LinkManager::closeLink()
{
    if(m_linkDevice)
    {
        m_linkDevice->close();
        emit linkOpenChanged(false);
        emit linkDeviceChanged(nullptr);
        delete m_linkDevice;
        m_linkDevice = nullptr;
//        qDebug()<<"Port closed";
    }
}

void LinkManager::openLink(int portIndex, qint32 baudRate)
{
    closeLink();

    if(portIndex >= 0 && portIndex < m_availablePortList.size())
    {
        QString portName = m_availablePortList.at(portIndex);

#ifdef Q_OS_ANDROID
        AndroidUsbBridgeSerialPort* serialPort = new AndroidUsbBridgeSerialPort();
#else // NOT Q_OS_ANDROID
        QSerialPort* serialPort = new QSerialPort(this);
#endif

        serialPort->setPortName(portName);

        if(!serialPort->open(QIODevice::ReadWrite))
        {
//            qDebug()<<"Opening port Failed";
            delete serialPort;
            return;
        }

//        qDebug()<<"Opening port Success";

#ifdef Q_OS_ANDROID
        serialPort->setParameters(baudRate,8,1,0);
#else // NOT Q_OS_ANDROID
        serialPort->setBaudRate(baudRate);
#endif

        m_linkDevice = serialPort;
        emit linkDeviceChanged(m_linkDevice);
        emit linkOpenChanged(true);
    }
}

void LinkManager::rescanAvailablePorts()
{
#ifdef Q_OS_ANDROID

    QStringList updatedPortList = AndroidUsbBridgeSerialPort::scanForAvailablePorts();

#else // NOT Q_OS_ANDROID

    //Build a string list containing port names
    QList<QSerialPortInfo> portInfoList = QSerialPortInfo::availablePorts();
    QStringList updatedPortList;
    for(QList<QSerialPortInfo>::const_iterator i = portInfoList.cbegin();
        i < portInfoList.cend();
        i++)
    {
        updatedPortList.append(i->portName());
    }

#endif

    if(updatedPortList != m_availablePortList)
    {
//        qDebug() << "Available ports changed :" << updatedPortList;
        m_availablePortList = updatedPortList;
        emit portListUpdated();
    }
}
