#-------------------------------------------------
#
# Project created by QtCreator 2017-01-29T18:34:14
#
#-------------------------------------------------

QT       += core gui quick serialport

android:include(android/android.pri)

TRANSLATIONS = gui/lang/fr_FR.ts

TARGET = grbl-overseer
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += main.cpp\
    gcode/gcodejob.cpp \
    gcode/gcodejobline.cpp \
    gcode/gcodejobmanager.cpp \
    gcode/gcodeparser.cpp \
    grbl/grblstringregister.cpp \
    grbl/grblboard.cpp \
    grbl/grblinstruction.cpp \
    grbl/grblengine.cpp \
    grbl/grblmessage.cpp \
    grbl/grblhistorylogic.cpp \
    grbl/grblconfiglogic.cpp \
    grbl/grblstatuslogic.cpp \
    grbl/grblbuildinfoslogic.cpp \
    grbl/activities/grblabstractactivity.cpp \
    grbl/activities/grbljobactivity.cpp \
    grbl/activities/grbljogactivity.cpp \
    grbl/activities/grblinstqueueactivity.cpp \
    visualizer/visualizeritem.cpp \
    visualizer/visualizeritemrenderer.cpp \
    visualizer/renderablejob.cpp \
    visualizer/renderableobject.cpp \
    visualizer/renderableaxes.cpp \
    visualizer/visualizerevent.cpp \
    visualizer/scopedbinders.cpp \
    visualizer/touchpointsmanager.cpp \
    visualizer/renderablemachinespace.cpp \
    visualizer/renderablespindle.cpp \
    visualizer/renderablejobhighlighter.cpp \
    link/linkmanager.cpp \
    applicationlogic.cpp \
    custommessagehandler.cpp \
    visualizer/renderablecenterindicator.cpp \
    visualizer/renderablerotationindicator.cpp

HEADERS  += \
    gcode/gcodeparser.h \
    gcode/gcodejobline.h \
    gcode/gcodedefinitions.h \
    gcode/gcodejobmanager.h \
    gcode/gcodejob.h \
    grbl/grblstringregister.h \
    grbl/grblboard.h \
    grbl/grblinstruction.h \
    grbl/grbldefinitions.h \
    grbl/grblengine.h \
    grbl/grblmessage.h \
    grbl/grblhistorylogic.h \
    grbl/grblconfiglogic.h \
    grbl/grblstatuslogic.h \
    grbl/grblconfigiface.h \
    grbl/grblhistoryiface.h \
    grbl/grblstatusiface.h \
    grbl/grblbuildinfoslogic.h \
    grbl/grblbuildinfosiface.h \
    grbl/activities/grblabstractactivity.h \
    grbl/activities/grbljobactivity.h \
    grbl/activities/grbljogactivity.h \
    grbl/activities/grblinstqueueactivity.h \
    visualizer/visualizeritem.h \
    visualizer/visualizeritemrenderer.h \
    visualizer/renderablejob.h \
    visualizer/renderableobject.h \
    visualizer/renderableaxes.h \
    visualizer/visualizerevent.h \
    visualizer/scopedbinders.h \
    visualizer/touchpointsmanager.h \
    visualizer/renderablemachinespace.h \
    visualizer/renderingconfig.h \
    visualizer/renderablespindle.h \
    visualizer/renderablejobhighlighter.h \
    link/linkmanager.h \
    applicationlogic.h \
    custommessagehandler.h \
    visualizer/renderablecenterindicator.h \
    visualizer/renderablerotationindicator.h


FORMS    +=

RESOURCES += \
    gui/resources.qrc

RC_ICONS = gui/icons/icon.ico

lupdate_only{
SOURCES += \
    gui/CustomButton.qml \
    gui/GRBLSettingsDelegate.qml \
    gui/ItemGroup.qml \
    gui/main.qml \
    gui/PanelSettings.qml \
    gui/TextWithCaption.qml \
    gui/CustomComboBox.qml \
    gui/HistoryDelegate.qml \
    gui/JobDelegate.qml \
    gui/PanelControls.qml \
    gui/ScrollBar.qml \
    gui/TopBar.qml \
    gui/CustomDialog.qml \
    gui/Indicator.qml \
    gui/JogControls.qml \
    gui/PanelJobs.qml \
    gui/SidePanel.qml \
    gui/DeployableItem.qml \
    gui/InputDisplay.qml \
    gui/JogPad.qml \
    gui/PanelMonitor.qml \
    gui/Style.qml
}
