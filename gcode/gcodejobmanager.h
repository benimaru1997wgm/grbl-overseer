#ifndef GCODEJOBMANAGER_H
#define GCODEJOBMANAGER_H

#include "gcodejobline.h"

#include <grbl/grblboard.h>

#include <QAbstractListModel>
#include <QList>
#include <QUrl>
#include <QColor>

class GrblBoard;
class GCodeJob;

class GCodeJobManager : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(int jobCount READ rowCount NOTIFY jobCountChanged)
    Q_PROPERTY(float totalProgression READ totalProgression NOTIFY totalProgressionChanged)

public:
    enum ItemRoles
    {
        TextRole  = Qt::UserRole + 1,
        ColorRole,
        ProgressRole,
        LineCountRole,
        CurrentLineRole,
        EstimatedTimeRole,
        RemainingTimeRole,
        StateRole,
        OriginRole,
    };

    explicit GCodeJobManager(QObject *parent = 0);

    int jobCount();
    float totalProgression();
    GCodeJob *getNextJobToRun();
    void prepareAllJobs();
    void unflagAllJobLines();

public://QAbstractListModel interface
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

public slots:
    void addJobs(QList<QUrl> urlList);
    void addJob(QUrl url);

    void removeJob(int index);
    void changeJobColor(int index);
    void toggleJobState(int index);
    void setJobOrigin(int index, QVector3D origin);

    void selectJob(int index);

    void onBoardSeekRateChanged(QVector3D seekRate);
    void setDefaultJobOrigin(QVector3D defaultOrigin);

signals:
    void jobCountChanged();
    void totalProgressionChanged();
    void jobLoadFailed(QString error);

    void jobAdded(GCodeJob* job);
    void jobRemoved(GCodeJob* job);
    void jobLineStatusChanged(GCodeJob* job, int line, GCodeJobLine::Status lineStatus);
    void jobPositionChanged(GCodeJob* job, QVector3D position);
    void jobColorChanged(GCodeJob* job, QColor color);
    void selectedJobChanged(GCodeJob* job);

private slots:
    void updateTotalProgression();

protected:
    void notifyOfModelChanges(GCodeJob* job, QVector<int> roles = QVector<int>());
    bool jobIndexExists(int index);
    QHash<int, QByteArray> roleNames() const;
    QColor getRandomColor();

private:
    QList<GCodeJob*> m_jobs;

    GCodeJob* m_selectedJobPtr;

    QVector3D m_boardSeekRate;
    QVector3D m_defaultJobOrigin;

    float m_totalProgression;
};

#endif // GCODEJOBMANAGER_H
