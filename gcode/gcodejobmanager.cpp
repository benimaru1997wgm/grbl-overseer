#include "gcodejobmanager.h"

#include "gcodejob.h"
#include "gcodejobline.h"

#include <grbl/grblboard.h>

#include <QDebug>


GCodeJobManager::GCodeJobManager(QObject *parent) :
    QAbstractListModel(parent),
    m_selectedJobPtr(nullptr),
    m_boardSeekRate(0.0f,0.0f,0.0f),
    m_totalProgression(0.0f)
{
    connect(this, &GCodeJobManager::jobCountChanged,
            this, &GCodeJobManager::updateTotalProgression);
}

int GCodeJobManager::jobCount()
{
    return m_jobs.size();
}

float GCodeJobManager::totalProgression()
{
    return m_totalProgression;
}

GCodeJob *GCodeJobManager::getNextJobToRun()
{
    for(QList<GCodeJob*>::iterator i = m_jobs.begin();
        i != m_jobs.end();
        i++)
    {
        if((*i)->getState() == GCodeJob::Job_Queued)
        {
            return (*i);
        }
    }

    return nullptr;
}

void GCodeJobManager::prepareAllJobs()
{
    for(QList<GCodeJob*>::iterator i = m_jobs.begin();
        i != m_jobs.end();
        i++)
    {
        (*i)->prepare();
    }
}

void GCodeJobManager::unflagAllJobLines()
{
    for(QList<GCodeJob*>::iterator i = m_jobs.begin();
        i != m_jobs.end();
        i++)
    {
        (*i)->unflagAllLines();
    }
}

void GCodeJobManager::addJob(QUrl url)
{
    GCodeJob *createdJob = new GCodeJob(url.toLocalFile());
    GCodeJob::GCodeJobState jobState = createdJob->getState();

    if(jobState == GCodeJob::Job_Ready){
        createdJob->setColor(getRandomColor());

        beginInsertRows(QModelIndex(), rowCount(), rowCount());

        createdJob->updateEstimatedMachineTimes(m_boardSeekRate);

        createdJob->setOrigin(m_defaultJobOrigin);

        m_jobs.append(createdJob);

        connect(createdJob, &GCodeJob::originChanged, [=](GCodeJob* job) {
            notifyOfModelChanges(job, {OriginRole});
        });

        connect(createdJob, &GCodeJob::stateChanged, [=](GCodeJob* job) {
            notifyOfModelChanges(job, {StateRole});
        });

        connect(createdJob, &GCodeJob::currentLineChanged, [=](GCodeJob* job)  {
            notifyOfModelChanges(job, {CurrentLineRole});
        });

        connect(createdJob, &GCodeJob::remEstMachTimeChanged, [=](GCodeJob* job) {
            notifyOfModelChanges(job, {RemainingTimeRole});
        });

        connect(createdJob, &GCodeJob::totEstMachTimeChanged, [=](GCodeJob* job) {
            notifyOfModelChanges(job, {EstimatedTimeRole});
        });

        connect(createdJob, &GCodeJob::progressionChanged, [=](GCodeJob* job) {
            notifyOfModelChanges(job, {ProgressRole});
        });

        connect(createdJob, &GCodeJob::colorChanged, [=](GCodeJob* job) {
            notifyOfModelChanges(job, {ColorRole});
        });

        connect(createdJob, &GCodeJob::originChanged,
                this, &GCodeJobManager::jobPositionChanged);
        connect(createdJob, &GCodeJob::lineStatusUpdated,
                this, &GCodeJobManager::jobLineStatusChanged);
        connect(createdJob, &GCodeJob::colorChanged,
                this, &GCodeJobManager::jobColorChanged);
        connect(createdJob, &GCodeJob::remEstMachTimeChanged,
                this, &GCodeJobManager::updateTotalProgression);
        connect(createdJob, &GCodeJob::totEstMachTimeChanged,
                this, &GCodeJobManager::updateTotalProgression);

        endInsertRows();

        emit jobAdded(createdJob);
        emit jobCountChanged();
        return;
    }

    switch(jobState)
    {
    case GCodeJob::Job_CouldNotOpenFile:
        emit jobLoadFailed("Could not load job " + createdJob->getPath() + " : file unavailable");
        break;

    case GCodeJob::Job_LineTooLong:
        emit jobLoadFailed(QString("Could not load job " + createdJob->getPath() + " : line %1 too long").arg(createdJob->getLineCount()));
        break;

    case GCodeJob::Job_Empty:
        emit jobLoadFailed("Could not open file " + createdJob->getPath() + " : unknown error");
        break;

    case GCodeJob::Job_ContainsCoordOffset:
        emit jobLoadFailed("Could not open file " + createdJob->getPath() + " : contains unsupported coordinate offset commands");
        break;

    default:
        emit jobLoadFailed("Could not open file " + createdJob->getPath() + " : even more unknown error");
        break;
    }

    delete(createdJob);
}

void GCodeJobManager::removeJob(int index)
{
    if(jobIndexExists(index)
        && (m_jobs.at(index)->getState() != GCodeJob::Job_Running))
    {
        beginRemoveRows(QModelIndex(), index, index);

        GCodeJob* removedJob = m_jobs.takeAt(index);

        if(removedJob == m_selectedJobPtr)
        {
            m_selectedJobPtr = nullptr;
            emit selectedJobChanged(m_selectedJobPtr);
        }

        endRemoveRows();

        emit jobRemoved(removedJob);

        delete removedJob;

        emit jobCountChanged();
    }
}

void GCodeJobManager::changeJobColor(int index)
{
    if(jobIndexExists(index))
    {
        m_jobs.at(index)->setColor(getRandomColor());
    }
}

void GCodeJobManager::toggleJobState(int index)
{
    if(jobIndexExists(index))
    {
        m_jobs.at(index)->toggleState();
    }
}

void GCodeJobManager::setJobOrigin(int index, QVector3D origin)
{
    if(jobIndexExists(index))
    {
        m_jobs.at(index)->setOrigin(origin);
    }
}

void GCodeJobManager::selectJob(int index)
{
    GCodeJob* selectedJobPtr = jobIndexExists(index) ? m_jobs.at(index) : nullptr;

    if(selectedJobPtr != m_selectedJobPtr)
    {
        m_selectedJobPtr = selectedJobPtr;
        emit selectedJobChanged(selectedJobPtr);
    }
}


bool GCodeJobManager::jobIndexExists(int index)
{
    return (index >= 0) && (index < m_jobs.size());
}

void GCodeJobManager::onBoardSeekRateChanged(QVector3D seekRate)
{
    m_boardSeekRate = seekRate;
    foreach(GCodeJob *job, m_jobs)
    {
        job->updateEstimatedMachineTimes(m_boardSeekRate);
    }
}

void GCodeJobManager::setDefaultJobOrigin(QVector3D defaultOrigin)
{
    m_defaultJobOrigin = defaultOrigin;
//    qDebug() << "Default job origin changed" <<m_defaultJobOrigin;
}

void GCodeJobManager::updateTotalProgression()
{
    quint32 totalEstimatedMachineTime = 0;
    quint32 remainingEstimatedMachineTime = 0;

    for(QList<GCodeJob*>::const_iterator jobIt = m_jobs.cbegin() ;
        jobIt != m_jobs.cend() ;
        jobIt++)
    {
        totalEstimatedMachineTime += (*jobIt)->getTotEstMachTime();
        remainingEstimatedMachineTime += (*jobIt)->getRemEstMachTime();
    }

    float totalProgression = 1.0f -
            ((float)remainingEstimatedMachineTime / (float)totalEstimatedMachineTime);

    if(totalProgression != m_totalProgression)
    {
        m_totalProgression = totalProgression;
        emit totalProgressionChanged();
    }
}

//##############################################################################
//
//                 UNDER THIS LINE IS THE MODEL RELATED CODE
//
//##############################################################################

void GCodeJobManager::notifyOfModelChanges(GCodeJob* job, QVector<int> roles)
{

    int updatedJobIndex = m_jobs.indexOf(job);

    if(updatedJobIndex >= 0)
    {
        emit dataChanged(createIndex(updatedJobIndex,0),
                         createIndex(updatedJobIndex,0),
                         roles);
    }

}

int GCodeJobManager::rowCount(const QModelIndex & parent) const
{
    return parent.isValid() ? 0 : m_jobs.count();
}

QVariant GCodeJobManager::data(const QModelIndex & index, int role) const
{
    int itemIndex = index.row();

    if(index.parent().isValid()
            || index.column() != 0
            || itemIndex >= m_jobs.size())
    {
        return QVariant();
    }

    GCodeJob* job = m_jobs.at(itemIndex);

    switch(role)
    {
    case Qt::DisplayRole:
    case TextRole:
        return job->getPath();

    case ColorRole:
        return job->getColor();

    case ProgressRole:
        return job->getProgression();

    case LineCountRole:
        return job->getLineCount();

    case CurrentLineRole:
        return job->getCurrentLineNumber() +1 ;

    case EstimatedTimeRole:
        return job->getTotEstMachTime();

    case RemainingTimeRole:
        return job->getRemEstMachTime();

    case StateRole:
        return job->getState();

    case OriginRole:
        return job->getOrigin();

    default:
        return QVariant();
    }
}

void GCodeJobManager::addJobs(QList<QUrl> urlList)
{
    for(QList<QUrl>::const_iterator i = urlList.cbegin();
        i != urlList.cend();
        i++)
    {
        addJob(*i);
    }
}


QHash<int, QByteArray> GCodeJobManager::roleNames() const
{
    return QHash<int, QByteArray>(
    {
        {TextRole,          "text"              },
        {ColorRole,         "color"             },
        {ProgressRole,      "progress"          },
        {LineCountRole,     "lineCount"         },
        {CurrentLineRole,   "currentLine"       },
        {EstimatedTimeRole, "estimatedTime"     },
        {RemainingTimeRole, "remainingTime"     },
        {StateRole,         "jobState"          },
        {OriginRole,        "jobOrigin"         },
    });
}

QColor GCodeJobManager::getRandomColor()
{
    return QColor::fromHsv(qrand() % 360,    //random hue
                           255,              //saturation
                           200);             //value
}
