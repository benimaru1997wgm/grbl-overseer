#include "gcodeparser.h"
#include "gcodedefinitions.h"

#include <QRegularExpression>
#include <QVector>
#include <QVector2D>
#include <qmath.h>

#define ANGLE_QUANTUM_DEG           5.0f

const QRegularExpression GCodeParser::s_whitespaceExpression =
        QRegularExpression("\\s+",QRegularExpression::OptimizeOnFirstUsageOption);

const QRegularExpression GCodeParser::s_wordBeginExpression =
        QRegularExpression("[A-Z]",QRegularExpression::OptimizeOnFirstUsageOption);

const GCodeParser::Axes GCodeParser::s_axisArray[3][3] = {
    {GCodeParser::Axis_X, GCodeParser::Axis_Y, GCodeParser::Axis_Z},
    {GCodeParser::Axis_Z, GCodeParser::Axis_X, GCodeParser::Axis_Y},
    {GCodeParser::Axis_Y, GCodeParser::Axis_Z, GCodeParser::Axis_X}
};

GCodeParser::GCodeParser()
{
    reset();
}

void GCodeParser::reset(){
    m_gcodeParserStatus.g0NonModal    = NON_MODAL_NO_ACTION;
    m_gcodeParserStatus.g1Motion      = MOTION_MODE_SEEK;
    m_gcodeParserStatus.g2Plane       = PLANE_SELECT_XY;
    m_gcodeParserStatus.g3Distance    = DISTANCE_MODE_ABSOLUTE;
    m_gcodeParserStatus.g6Units       = UNITS_MODE_MM;
    m_gcodeParserStatus.machineRate   = 0.0f;
    m_gcodeParserStatus.currentPos    = QVector3D();

    m_validAxisHelper.invalidateAllAxes();

    m_wordMap.clear();

    m_lastLinePathPoints.clear();
}


void GCodeParser::processLine(QString gcodeString){

    splitLineIntoWords(gcodeString);
    executeWords();

    m_wordMap.clear();
    m_gcodeParserStatus.g0NonModal = NON_MODAL_NO_ACTION;
}


void GCodeParser::splitLineIntoWords(QString gcodeString){

    //Simplify gcode
    QString simplifiedGCode = gcodeString.toUpper();
    simplifiedGCode.remove(s_whitespaceExpression);       //Removes whitespaces (incl line return characters)

    //Find first gcode word (should be at index 0, but better test it)
    int index = simplifiedGCode.indexOf(s_wordBeginExpression);

    //If no gcode word found, no need to try to parse this line
    if(index<0){
        return;
    }

    while(index < simplifiedGCode.size()){
        //Extract letter
        char letter = simplifiedGCode.at(index).toLatin1();

        //Now we will use index+1 as reference (the character after the beginning of the Gcode word)
        index++;

        //Locate next gcode word
        int nextIndex = simplifiedGCode.indexOf(s_wordBeginExpression,index);

        //If no next index found, next index is the end of the string
        if(nextIndex<0){
            nextIndex=simplifiedGCode.size();
        }

        //Extract value
        QStringRef valueStringRef = simplifiedGCode.midRef(index,nextIndex-index);
        bool success = true;
        float value = valueStringRef.toFloat(&success);

        //If parsing went successful, add this word to map
        if(success){
            m_wordMap.insert(letter,value);
        }

        //Get ready to locate next word
        index = nextIndex;
    }
}

void GCodeParser::executeWords(){
    m_lastLinePathPoints.clear();

    //Process 'F' words
    foreach(float value,m_wordMap.values('F')){
        //Convert it to mm
        if(m_gcodeParserStatus.g6Units == UNITS_MODE_INCHES){
            value *= MM_PER_INCH;
        }
        m_gcodeParserStatus.machineRate = value / 60.0f; //was mm/m, now into mm/s
    }

    //Process 'G' words
    processGValues();

    //Before updating axes positions, we must save this
    bool allAxesWereAlreadyValid = m_validAxisHelper.areAllAxesValid();

    //Process 'X', 'Y' and 'Z' words and update axes positions
    QVector3D targetPos = processXYZValues();

    //If no movements to do, or not all axis valid : no need to plot movement
    if(allAxesWereAlreadyValid
    && ( targetPos != m_gcodeParserStatus.currentPos ) )
    {
        switch (m_gcodeParserStatus.g1Motion) {
        case MOTION_MODE_LINEAR:
        case MOTION_MODE_SEEK:
            m_lastLinePathPoints = buildLinePointsVector(targetPos);
            break;

        case MOTION_MODE_CCW_ARC:
        case MOTION_MODE_CW_ARC:
            m_lastLinePathPoints = buildArcPointsVector(targetPos);
            break;

        default:
            break;
        }
    }

    m_gcodeParserStatus.currentPos=targetPos;
}

QVector<QVector3D> GCodeParser::buildLinePointsVector(QVector3D target){
    QVector<QVector3D> pointsVector;
    pointsVector.append(m_gcodeParserStatus.currentPos);
    pointsVector.append(target);

    return pointsVector;
}

QVector<QVector3D> GCodeParser::buildArcPointsVector(QVector3D target){
    const Axes *axis = getAxisMap();
    QVector<QVector3D> pointsVector;

    //Retrieve points in plane
    QVector2D center2DPos = computeArcCenter(target);
    QVector2D start2DPos( m_gcodeParserStatus.currentPos[axis[0]],
                          m_gcodeParserStatus.currentPos[axis[1]]);
    QVector2D end2DPos( target[axis[0]],
                        target[axis[1]]);

    float radius = start2DPos.distanceToPoint(center2DPos);

    //Ensure center is equidistant from start and end point ( will catch errors in arcCenter computation )
    float radiusEnd = end2DPos.distanceToPoint(center2DPos);
    if(qAbs(radius - radiusEnd) >  ARC_ERROR){
        return pointsVector;
    }

    //Get angles
    float startAngle =  qAtan2( start2DPos.y()-center2DPos.y(),
                                start2DPos.x()-center2DPos.x());
    float endAngle =    qAtan2( end2DPos.y()-center2DPos.y(),
                                end2DPos.x()-center2DPos.x());
    float deltaAngle = (m_gcodeParserStatus.g1Motion == MOTION_MODE_CCW_ARC)
            ? endAngle-startAngle
            : startAngle-endAngle;

    //Get drill heights
    float startHeight = m_gcodeParserStatus.currentPos[axis[2]];
    float endHeight = target[axis[2]];
    float deltaHeight =  endHeight - startHeight;

    //Ensure angles are correct, and take number of turn into account
    float revolutionCount = qAbs(m_wordMap.value('P',0.0f));
    if(deltaAngle < 0){
        revolutionCount += 1;
    }
    deltaAngle += 2.0f * M_PI* revolutionCount;


    //Start building points
    pointsVector.append(m_gcodeParserStatus.currentPos);

    for(float angle = 0.0f ; angle < deltaAngle ; angle += ANGLE_QUANTUM_DEG * M_PI /180.0f){
        float currentAngle = (m_gcodeParserStatus.g1Motion == MOTION_MODE_CCW_ARC)
                ? startAngle + angle
                : startAngle - angle;

        QVector2D point2D = center2DPos;
        point2D[0] += radius * qCos(currentAngle);
        point2D[1] += radius * qSin(currentAngle);

        float height = startHeight + deltaHeight * angle / deltaAngle;

        QVector3D point3D;
        point3D[axis[0]]=point2D.x();
        point3D[axis[1]]=point2D.y();
        point3D[axis[2]]= height;

        pointsVector.append(point3D);
    }

    pointsVector.append(target);

    return pointsVector;
}

QVector2D GCodeParser::computeArcCenter(QVector3D target){
    const Axes *axis = getAxisMap();

    //Initialize center position at current position
    QVector2D arcCenter;
    arcCenter[0] = m_gcodeParserStatus.currentPos[axis[0]];
    arcCenter[1] = m_gcodeParserStatus.currentPos[axis[1]];


    //try with radius definition
    if(m_wordMap.contains('R')){
        float r = m_wordMap.value('R');

        if(m_gcodeParserStatus.g6Units == UNITS_MODE_INCHES){
            r *= MM_PER_INCH;
        }

        //From GRBL code :
        //d -> sqrt(x^2 + y^2)
        //h -> sqrt(4 * r^2 - x^2 - y^2)/2
        //i -> (x - (y * h) / d) / 2
        //j -> (y + (x * h) / d) / 2

        //Actual computation
        float x = m_gcodeParserStatus.currentPos[axis[0]] - target[axis[0]];
        float y = m_gcodeParserStatus.currentPos[axis[1]] - target[axis[1]];

        float d = qSqrt((x*x) - (y*y));

        float h = qSqrt((4.0f*r*r) - (x*x) - (y*y));

        //Error
        if(h < 0) {
            return arcCenter;
        }

        if(m_gcodeParserStatus.g1Motion == MOTION_MODE_CW_ARC){
            h = -h;     //This cheat is from grbl code
        }

        float i = (x - (y * h) / d) / 2;
        float j = (y + (x * h) / d) / 2;

        arcCenter[axis[0]] +=i;
        arcCenter[axis[1]] +=j;
    }


    //Try with center offset definition
    else{
        const char axisLetter[] = {'I','J','K'};
        for(int i = 0 ; i < 2 ; i++){
            float value = m_wordMap.value(axisLetter[axis[i]],0.0f);

            if(m_gcodeParserStatus.g6Units == UNITS_MODE_INCHES){
                value *= MM_PER_INCH;
            }

            arcCenter[i] += value;
        }
    }

    return arcCenter;
}

void GCodeParser::processGValues(){
    foreach(float value,m_wordMap.values('G')){

        int mantissa = qAbs(value);
        int decimal = qRound((value-mantissa)*10.0f);

        switch(mantissa){
        //G0_NonModalActions
        case 64:
            m_gcodeParserStatus.g0NonModal=NON_MODAL_DWELL;
            break;
        case 10:
            m_gcodeParserStatus.g0NonModal=NON_MODAL_SET_COORDINATE_DATA;
            break;
        case 28:
            switch(decimal){
            case 0:
                m_gcodeParserStatus.g0NonModal=NON_MODAL_GO_HOME_0;
                break;
            case 1:
                m_gcodeParserStatus.g0NonModal=NON_MODAL_SET_HOME_0;
                break;
            default:
                break;
            }
            break;
        case 30:
            switch(decimal){
            case 0:
                m_gcodeParserStatus.g0NonModal=NON_MODAL_GO_HOME_1;
                break;
            case 1:
                m_gcodeParserStatus.g0NonModal=NON_MODAL_SET_HOME_1;
                break;
            default:
                break;
            }
            break;
        case 53:
            m_gcodeParserStatus.g0NonModal=NON_MODAL_ABSOLUTE_OVERRIDE;
            break;
        case 92:
            switch(decimal){
            case 0:
                m_gcodeParserStatus.g0NonModal=NON_MODAL_SET_COORDINATE_OFFSET;
                break;
            case 1:
                m_gcodeParserStatus.g0NonModal=NON_MODAL_RESET_COORDINATE_OFFSET;
                break;
            default:
                break;
            }
            break;

            //G1_MotionModes
        case 0:
            m_gcodeParserStatus.g1Motion=MOTION_MODE_SEEK;
            break;
        case 1:
            m_gcodeParserStatus.g1Motion=MOTION_MODE_LINEAR;
            break;
        case 2:
            m_gcodeParserStatus.g1Motion=MOTION_MODE_CW_ARC;
            break;
        case 3:
            m_gcodeParserStatus.g1Motion=MOTION_MODE_CCW_ARC;
            break;
        case 38:
            m_gcodeParserStatus.g1Motion=MOTION_MODE_PROBE;
            break;
        case 80:
            m_gcodeParserStatus.g1Motion=MOTION_MODE_NONE;

            //G2_PlaneSelect
        case 17:
            m_gcodeParserStatus.g2Plane=PLANE_SELECT_XY;
            break;
        case 18:
            m_gcodeParserStatus.g2Plane=PLANE_SELECT_ZX;
            break;
        case 19:
            m_gcodeParserStatus.g2Plane=PLANE_SELECT_YZ;
            break;

            //G3_DistanceMode
        case 90:
            m_gcodeParserStatus.g3Distance=DISTANCE_MODE_ABSOLUTE;
            break;
        case 91:
            m_gcodeParserStatus.g3Distance=DISTANCE_MODE_INCREMENTAL;
            break;

            //G6_UnitsMode
        case 20:
            m_gcodeParserStatus.g6Units=UNITS_MODE_INCHES;
            break;
        case 21:
            m_gcodeParserStatus.g6Units=UNITS_MODE_MM;
            break;

        default:
            break;
        }
    }
}

QVector3D GCodeParser::processXYZValues(){
    const char axisLetters[] = {'X','Y','Z'};

    //Start from current position
    QVector3D targetPosition = m_gcodeParserStatus.currentPos;

    //For each axis
    for(quint8 currAxis = 0 ; currAxis < 3 ; currAxis++){
        const char currentAxisLetter = axisLetters[currAxis];

        //Only if this axis letter is mentionned in wordMap
        if(!m_wordMap.contains(currentAxisLetter)){
            continue;
        }

        //Get its value
        float axisValue = m_wordMap.value(currentAxisLetter);

        //Convert it to mm
        if(m_gcodeParserStatus.g6Units == UNITS_MODE_INCHES){
            axisValue *= MM_PER_INCH;
        }

        //Modify target position accordingly
        if(m_gcodeParserStatus.g3Distance == DISTANCE_MODE_ABSOLUTE
        || m_gcodeParserStatus.g0NonModal == NON_MODAL_ABSOLUTE_OVERRIDE){
            targetPosition[currAxis]=axisValue;        //Absolute coordinate
        }
        else{
            targetPosition[currAxis]+=axisValue;       //relative coordinate
        }

        //This axis position is now valid (does not only contain start position anymore)
        m_validAxisHelper.validateAxis(currAxis);

    }

    return targetPosition;
}

bool GCodeParser::getLastLineIsWork() const{
    switch(m_gcodeParserStatus.g1Motion){
    case MOTION_MODE_LINEAR:
    case MOTION_MODE_CCW_ARC:
    case MOTION_MODE_CW_ARC:
        return true;
    default:
        return false;
    }
}

bool GCodeParser::lastLineContainsCoordOffset() const
{
    switch(m_gcodeParserStatus.g0NonModal)
    {
    case NON_MODAL_SET_COORDINATE_OFFSET:
    case NON_MODAL_RESET_COORDINATE_OFFSET:
    case NON_MODAL_GO_HOME_0:
    case NON_MODAL_SET_HOME_0:
    case NON_MODAL_GO_HOME_1:
    case NON_MODAL_SET_HOME_1:
        return true;
    default:
        return false;
    }
}

const GCodeParser::Axes *GCodeParser::getAxisMap(){
    return s_axisArray[m_gcodeParserStatus.g2Plane];
}


//##############################################################################


void GCodeParser::AxesValidityHelper::invalidateAllAxes()
{
    m_validAxisFlags = 0x00;
}

bool GCodeParser::AxesValidityHelper::areAllAxesValid()
{
    return (m_validAxisFlags ==
            ((1 << GCodeParser::Axis_X) | (1 << GCodeParser::Axis_Y) | (1 << GCodeParser::Axis_Z))
            );
}

void GCodeParser::AxesValidityHelper::validateAxis(uint8_t axis)
{
    m_validAxisFlags |= (1<<axis);
}
