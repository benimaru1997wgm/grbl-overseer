#ifndef GCODEPARSER_H
#define GCODEPARSER_H

#include <QObject>
#include <QVector>
#include <QVector3D>
#include <QMultiMap>

class GCodeLine;

class GCodeParser
{
public:
    GCodeParser();

    void reset();

    void processLine(QString gcodeString);

    QVector<QVector3D> getLastLinePathPoints() const
        {return m_lastLinePathPoints;}

    QVector3D getCurrentPosition() const
        {return m_gcodeParserStatus.currentPos;}

    float getLastLineMachineRate() const
        {return m_gcodeParserStatus.machineRate;}

    bool getLastLineIsWork() const;

    bool lastLineContainsCoordOffset() const;

private:

    enum G0_NonModalActions{NON_MODAL_NO_ACTION = 0, NON_MODAL_DWELL, NON_MODAL_SET_COORDINATE_DATA, NON_MODAL_GO_HOME_0,
                            NON_MODAL_SET_HOME_0, NON_MODAL_GO_HOME_1, NON_MODAL_SET_HOME_1, NON_MODAL_ABSOLUTE_OVERRIDE,
                            NON_MODAL_SET_COORDINATE_OFFSET, NON_MODAL_RESET_COORDINATE_OFFSET};
    enum G1_MotionModes{MOTION_MODE_SEEK = 0, MOTION_MODE_LINEAR, MOTION_MODE_CW_ARC, MOTION_MODE_CCW_ARC,
                        MOTION_MODE_PROBE, MOTION_MODE_NONE};

    enum G2_PlaneSelect{PLANE_SELECT_XY = 0, PLANE_SELECT_ZX, PLANE_SELECT_YZ};

    enum G3_DistanceMode{DISTANCE_MODE_ABSOLUTE = 0, DISTANCE_MODE_INCREMENTAL};

    enum G6_UnitsMode{UNITS_MODE_MM = 0, UNITS_MODE_INCHES};

    enum Axes {
        Axis_X = 0,
        Axis_Y = 1,
        Axis_Z = 2
    };

    struct ParserStatusStruct {
        G0_NonModalActions  g0NonModal;     // being non modal, not sure it deserve to be here
        G1_MotionModes      g1Motion;
        G2_PlaneSelect      g2Plane;
        G3_DistanceMode     g3Distance;
        G6_UnitsMode        g6Units;
        float               machineRate;    // mm/s
        QVector3D           currentPos;     // mm
    };

    class AxesValidityHelper {
    public:
        bool areAllAxesValid();
        void invalidateAllAxes();
        void validateAxis(uint8_t axis);
    private:
        uint8_t m_validAxisFlags;
    };

    void splitLineIntoWords(QString gcodeString);
    void executeWords();
    void processGValues();
    QVector3D processXYZValues();

    QVector<QVector3D> buildLinePointsVector(QVector3D target);
    QVector<QVector3D> buildArcPointsVector(QVector3D target);
    QVector2D computeArcCenter(QVector3D target);

    const Axes *getAxisMap();

    ParserStatusStruct m_gcodeParserStatus;

    QVector<QVector3D> m_lastLinePathPoints;

    QMultiMap<char,float> m_wordMap;

    AxesValidityHelper m_validAxisHelper;   //Keeps track of "valid" (== not just start value) axis

    static const Axes s_axisArray[3][3];
    static const QRegularExpression s_whitespaceExpression;
    static const QRegularExpression s_wordBeginExpression;
};

#endif // GCODEPARSER_H
