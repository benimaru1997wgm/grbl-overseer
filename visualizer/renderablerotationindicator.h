#ifndef RENDERABLEROTATIONINDICATOR_H
#define RENDERABLEROTATIONINDICATOR_H

#include "renderableobject.h"

class RenderableRotationIndicator : public RenderableObject
{
public:
    RenderableRotationIndicator();

    void updatePositionAndRotation(QVector2D viewPortSize, QQuaternion rotation);
};

#endif // RENDERABLEROTATIONINDICATOR_H
