#ifndef RENDERABLEMACHINESPACE_H
#define RENDERABLEMACHINESPACE_H

#include "renderableobject.h"

#include <QVector3D>
#include <QVector>


class RenderableMachineSpace : public RenderableObject
{
public:
    RenderableMachineSpace();

    void setLimit(QVector3D position);

    void render(QOpenGLShaderProgram *program) override;

private:
    QVector3D m_currPosition;

    void rebuildGeometry();

    static float sqrtHalfSq(float a);
    static const QVector<GLushort> s_indices;
};

#endif // RENDERABLEMACHINESPACE_H
