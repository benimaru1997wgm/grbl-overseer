#include "touchpointsmanager.h"

#include <QtMath>
#include <QTimer>

#define DOUBLE_PRESS_DELAY    1000
#define DOUBLE_PRESS_RADIUS   50

TouchPointsManager::TouchPointsManager(QObject *parent) :
    QObject(parent),
    m_doublePressStage(DoublePress_Wait)
{
    //Should be more than 5 active touchpoints. Preallocate to avoid resize later
    m_touchPoints.reserve(5);

    m_doublePressTimer = new QTimer(this);
    m_doublePressTimer->setInterval(DOUBLE_PRESS_DELAY);
    m_doublePressTimer->setSingleShot(true);

    connect(m_doublePressTimer, &QTimer::timeout,
            [=]()
    {
        m_doublePressStage = DoublePress_Wait;
    });
}

void TouchPointsManager::processEvent(QTouchEvent *event)
{
    event->accept();

    //Maintain the list of active touchpoints
    for(QList<QTouchEvent::TouchPoint>::const_iterator i = event->touchPoints().cbegin();
        i != event->touchPoints().cend();
        i++)
    {
        switch(i->state())
        {
        case Qt::TouchPointPressed:
            appendTouchPoint(i->id(), QVector2D(i->pos()));
            break;

        case Qt::TouchPointReleased:
            removeTouchPoint(i->id());
            break;

        case Qt::TouchPointMoved:
            updateTouchPoint(i->id(),QVector2D(i->pos()));
            break;

        default:
            break;
        }
    }

    if(m_doublePressStage == DoublePress_Completed)
    {
        emit doublePress();
        m_doublePressStage = DoublePress_Notified;
        return;
    }
    else if(m_doublePressStage == DoublePress_Notified)
    {
        return;
    }
    else if((event->touchPointStates() & Qt::TouchPointMoved) != 0)
    {
        QVector2D panVector = getPanVector();
        if(!panVector.isNull())
        {
            emit panned(panVector);
        }

        float pinchScaleRatio = getPinchScaleRatio();
        if(pinchScaleRatio != 0.0f)
        {
            emit pinchScaled(pinchScaleRatio);
        }

        QVector2D translateVector = getPinchCenterMovementVector();
        if(!translateVector.isNull())
        {
            emit pinchMoved(translateVector);
        }

        float rotationAngle = getPinchRotationAngle();
        if(rotationAngle != 0.0f)
        {
            emit pinchRotated(rotationAngle);
        }
    }
}

void TouchPointsManager::appendTouchPoint(int id, QVector2D pos)
{
    m_touchPoints.append(TouchPointStruct(id,pos));

    //Double press
    if(m_touchPoints.size() == 1)
    {
        if(m_doublePressStage == DoublePress_Wait)
        {
            m_doublePressTimer->start();
            m_doublePressPos = pos;
            m_doublePressStage = DoublePress_Down;
        }

        else if(m_doublePressStage == DoublePress_Up
             && m_doublePressPos.distanceToPoint(pos) < DOUBLE_PRESS_RADIUS)
        {
            m_doublePressStage = DoublePress_Completed;
            m_doublePressTimer->stop();
        }
    }
    else if(m_touchPoints.size() > 1)
    {
        m_doublePressStage = DoublePress_Wait;
        m_doublePressTimer->stop();
    }

}

void TouchPointsManager::updateTouchPoint(int id, QVector2D pos)
{
    for(QVector<TouchPointStruct>::iterator i = m_touchPoints.begin();
        i != m_touchPoints.end();
        i++)
    {
        if(i->id == id)
        {
            i->prevPos = i->currPos;
            i->currPos = pos;
            break;
        }
    }
}

void TouchPointsManager::removeTouchPoint(int id)
{
    for(int i = 0 ; i < m_touchPoints.size() ; i ++)
    {
        if(m_touchPoints.at(i).id == id)
        {
            m_touchPoints.remove(i);
            break;
        }
    }

    //Double press
    if(m_touchPoints.size() == 0)
        if(m_doublePressStage == DoublePress_Down)
    {
        m_doublePressStage = DoublePress_Up;
    }

}


QVector2D TouchPointsManager::getPanVector()
{
    if (m_touchPoints.size() != 1)
    {
        return QVector2D();
    }

    return m_touchPoints.at(0).currPos - m_touchPoints.at(0).prevPos;
}

float TouchPointsManager::getPinchScaleRatio()
{
    if (m_touchPoints.size() != 2)
    {
        return 0.0f;
    }

    const TouchPointStruct &touchPointA = m_touchPoints.at(0);
    const TouchPointStruct &touchPointB = m_touchPoints.at(1);
    float currPointDistance = touchPointA.currPos.distanceToPoint(touchPointB.currPos);
    float prevPointDistance = touchPointA.prevPos.distanceToPoint(touchPointB.prevPos);
    return (currPointDistance == 0.0f) ? 0.0f : prevPointDistance / currPointDistance;

}

QVector2D TouchPointsManager::getPinchCenterMovementVector()
{
    if (m_touchPoints.size() != 2)
    {
        return QVector2D();
    }

    const TouchPointStruct &touchPointA = m_touchPoints.at(0);
    const TouchPointStruct &touchPointB = m_touchPoints.at(1);
    QVector2D curr2Center = touchPointA.currPos + touchPointB.currPos;
    QVector2D prev2Center = touchPointA.prevPos + touchPointB.prevPos;
    return (curr2Center - prev2Center) / 2.0f;
}

float TouchPointsManager::getPinchRotationAngle()
{
    if (m_touchPoints.size() != 2)
    {
        return 0.0f;
    }

    const TouchPointStruct &touchPointA = m_touchPoints.at(0);
    const TouchPointStruct &touchPointB = m_touchPoints.at(1);
    QVector2D currDirection = touchPointA.currPos - touchPointB.currPos;
    QVector2D prevDirection = touchPointA.prevPos - touchPointB.prevPos;

    //Compute the difference in angles
    float dotProduct = QVector2D::dotProduct(currDirection,prevDirection);
    float crossProduct = currDirection.x() * prevDirection.y() - currDirection.y() * prevDirection.x();
    float angle = qAtan2(crossProduct, dotProduct);

    //To degree
    angle *= 180.0f / M_PI;

    //qDebug() << "Rotate angle : " << angle;

    return angle;
}
