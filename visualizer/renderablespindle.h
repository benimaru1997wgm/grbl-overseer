#ifndef RENDERABLEDRILL_H
#define RENDERABLEDRILL_H

#include "renderableobject.h"

#include <QElapsedTimer>

class RenderableSpindle : public RenderableObject
{
public:
    RenderableSpindle(float height, float radius, quint8 detailLevel = 5);


    void setPosition(QVector3D position);
    void setRotation(float velocity);   //Between -1 (full CCW) and 1 (full CW)
    bool update();

private:
    QVector<VertexData> getSubdivVertices(float circlePosition,
                                               float height,
                                               float radius);

    QVector<GLushort> getSubdivIndices(quint16 currSubdivIndex);

    //Those only change when new position is received
    QElapsedTimer m_timeSincePosReceived;
    QVector3D m_prevPos;    // Position when last position was received
    QVector3D m_currPos;    // Current position
    QVector3D m_destPos;    // Target position
    QVector3D m_velocity;   // Movement vector

    QElapsedTimer m_timeSinceLastRotation;
    float m_rotAngle;
    float m_rotVelocity;

    GLsizei m_vertexCount;
};

#endif // RENDERABLEDRILL_H
