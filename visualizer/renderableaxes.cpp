#include "renderableaxes.h"
#include <QColor>

RenderableAxes::RenderableAxes():
    RenderableObject(1, 1)
{
    const QVector4D xAxisColor = colorToVector4D(Qt::red);
    const QVector4D yAxisColor = colorToVector4D(Qt::green);
    const QVector4D zAxisColor = colorToVector4D(Qt::blue);

    QVector<RenderableObject::VertexData> vertexData;
    vertexData.append({QVector3D(0,0,0), xAxisColor});
    vertexData.append({QVector3D(1,0,0), xAxisColor});
    vertexData.append({QVector3D(0,0,0), yAxisColor});
    vertexData.append({QVector3D(0,1,0), yAxisColor});
    vertexData.append({QVector3D(0,0,0), zAxisColor});
    vertexData.append({QVector3D(0,0,1), zAxisColor});

    createVertexBuffer(vertexData.size(), vertexData.data());

    RenderingConfig & mainRndrConfPtr = getRndrConfRef(0);
    mainRndrConfPtr.drawMode        = RenderingConfig::DrawMode_Lines;
    mainRndrConfPtr.vertexCount     = vertexData.size();
    mainRndrConfPtr.useIndices      = false;
    mainRndrConfPtr.colorMode       = RenderingConfig::ColorMode_DirectColor;
    mainRndrConfPtr.lineWidth       = 2.0f;
    mainRndrConfPtr.mMatrixIndex    = 0;
    mainRndrConfPtr.colorAFactor    = QVector4D(1.0f,1.0f,1.0f,1.0f);
    mainRndrConfPtr.colorBFactor    = QVector4D(0.0f,0.0f,0.0f,0.0f);
}
