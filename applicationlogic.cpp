#include "applicationlogic.h"

#include <gcode/gcodejobmanager.h>
#include <grbl/grblboard.h>

#include <QDebug>

ApplicationLogic::ApplicationLogic(GCodeJobManager *jobManager, GrblBoard *grblBoard, QObject *parent) :
    QObject(parent),
    m_currentState(State_Idle),
    m_readyToGo(false),
    m_jobManager(jobManager),
    m_grblBoard(grblBoard)
{
    connect(m_grblBoard, &GrblBoard::jobCompleted,
            this, &ApplicationLogic::onGrblJobCompleted);
    connect(m_grblBoard, &GrblBoard::isSimulationModeChanged,
            this, &ApplicationLogic::startIfGrblIsInCorrectMode);
    connect(m_grblBoard, &GrblBoard::error,
            this, &ApplicationLogic::onGrblError);

    //ReadyToGo :
    connect(this, &ApplicationLogic::isIdleChanged,
            this, &ApplicationLogic::updateReadyToGoFlag);
    connect(m_grblBoard, &GrblBoard::isIdleChanged,
            this ,&ApplicationLogic::updateReadyToGoFlag);
    connect(m_jobManager ,&GCodeJobManager::jobCountChanged,
            this, &ApplicationLogic::updateReadyToGoFlag);
}

void ApplicationLogic::startSimulation()
{
//    qDebug() << "Start simulation";
    m_errorsRegistry.clear();
    m_jobManager->prepareAllJobs();
    m_jobManager->unflagAllJobLines();
    setState(State_WaitingForSimulationMode);
    m_grblBoard->setSimulationMode(true);
    startIfGrblIsInCorrectMode();
}

void ApplicationLogic::startProduction()
{
    m_errorsRegistry.clear();
    m_jobManager->prepareAllJobs();
    setState(State_WaitingForProductionMode);
    m_grblBoard->setSimulationMode(false);
    startIfGrblIsInCorrectMode();
}

void ApplicationLogic::abort()
{
    //qDebug() << "Abort";
    setState(State_Idle);
    m_grblBoard->reset();
    m_errorsRegistry.clear();
}

void ApplicationLogic::setJobOriginAtCurrentPosition(int jobIndex)
{
    if(readyToGo())
    {
        m_jobManager->setJobOrigin(jobIndex, m_grblBoard->status()->getMachinePositionInMm());
    }
}

void ApplicationLogic::onGrblJobCompleted()
{
    runNextJob();
}

void ApplicationLogic::startIfGrblIsInCorrectMode()
{
    //qDebug() << "Start if in correct mode";
    if(m_currentState == State_WaitingForProductionMode
    && !m_grblBoard->isSimulationMode())
    {
        //qDebug() << "Start production";
        setState(State_Production_Running);
        runNextJob();

    }
    else if(m_currentState == State_WaitingForSimulationMode
                && m_grblBoard->isSimulationMode())
    {
        //qDebug() << "Start simulation";
        setState(State_Simulation_Running);
        runNextJob();
    }
}

void ApplicationLogic::onGrblError(QString errorMessage)
{
    if(m_grblBoard->getCurrentJob() &&
    ( m_currentState == State_Simulation_Running || m_currentState == State_Production_Running))
    {
        m_errorsRegistry.append(errorMessage);

        if(m_currentState == State_Production_Running)
        {
            emit productionError(errorMessage);
        }
    }
}

void ApplicationLogic::updateReadyToGoFlag()
{
    bool readyToGo = isIdle() && m_grblBoard->isIdle() && m_jobManager->jobCount() > 0;

    if(m_readyToGo != readyToGo)
    {
        m_readyToGo = readyToGo;
        emit readyToGoChanged();
    }
}

void ApplicationLogic::runNextJob()
{
    //qDebug() << "RunNextJob";

    GCodeJob* nextJob = m_jobManager->getNextJobToRun();

    if(nextJob)
    {
        //Run job
        m_grblBoard->runJob(nextJob);
    }
    else {
        switch (m_currentState) {
        case State_Simulation_Running:
            setState(State_Simulation_Completed);
            emit simulationCompleted();
            break;

        case State_Production_Running:
            setState(State_Production_Completed);
            emit productionCompleted();
            break;

        default:
            break;
        }
    }
}

void ApplicationLogic::setState(ApplicationLogic::State state)
{
    //qDebug() << "SetState";

    if(state != m_currentState)
    {
        bool changedIdle = (state == State_Idle || m_currentState == State_Idle);
        m_currentState = state;
        emit currentStateChanged();
        if(changedIdle)
        {
            emit isIdleChanged();
        }
    }
}
